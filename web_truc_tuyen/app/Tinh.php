<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tinh extends Model
{
    //
    protected $table='tinh';
    public function thanhpho_huyen(){
        return $this->hasMany('App\ThanhPhoHuyen','id_tinh','id');
    }
}
