<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThiTranXa extends Model
{
    //
    protected $table='thitran_xa';
    public function ho_gia_dinh(){
        return $this->hasMany('App\HoGiaDinh','id_thitra_xa','id');
    }
    public function thanhpho_huyen(){
        return$this->belongsTo('App\ThanhPhoHuyen','id_thanhpho_huyen','id');
    }
}
