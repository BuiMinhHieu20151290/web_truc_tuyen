<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThanhPhoHuyen extends Model
{
    //
    protected $table='thanhpho_huyen';
    public function thitran_xa(){
        return $this->hasMany('App\ThiTranXa','id_thanhpho_huyen','id');
    }
    public function tinh(){
        return $this->belongsTo('App\Tinh','id_tinh','id');
    }
}
