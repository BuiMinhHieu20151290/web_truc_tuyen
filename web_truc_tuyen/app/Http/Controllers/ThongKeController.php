<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ThongKeController extends Controller
{
    //
    public function getDanhSachCongDan17(){
        $dsTinh= DB::table('tinh')->get();
        return view('user.page.congdan17',['dsTinh'=>$dsTinh]);
    }
    public function loadDanhSachCongDan17(Request $req){
        $id = $req->id;
        $list_cong_dan = DB::table('cong_dan')->join('ho_gia_dinh','ho_gia_dinh.id','=','cong_dan.id_ho_gia_dinh')->join('thitran_xa','thitran_xa.id','=','ho_gia_dinh.id_thitran_xa')->where('thitran_xa.id','=',$id)->select('cong_dan.id','cong_dan.ho_ten','cong_dan.ngay_sinh','cong_dan.que_quan','ho_gia_dinh.sohokhau')->get();
        $result = array();
        $current_year = date('Y')-16;
        foreach ($list_cong_dan as $congdan){
            $year_born = $congdan -> ngay_sinh;
            $split = explode('-',$year_born)[0];
            if($split == $current_year){
                array_push($result, $congdan);
            }
        }
        return view('user.item.danhsach17',['listCongDan'=>$result])->render();
    }
    public function getTiLeDanSo(){
        $ti_le_ds = DB::table('cong_dan')->select('gioi_tinh', DB::raw('count(*) as total'))->groupBy('gioi_tinh')->get();
        $tinhs = DB::table('tinh')->get();
        $ti_le_dan = DB::table('tinh')->join('thanhpho_huyen', 'thanhpho_huyen.id_tinh', '=', 'tinh.id')->join('thitran_xa','thitran_xa.id_thanhpho_huyen', '=', 'thanhpho_huyen.id')->join('ho_gia_dinh', 'ho_gia_dinh.id_thitran_xa', '=', 'thitran_xa.id')->join('cong_dan','cong_dan.id_ho_gia_dinh', '=','ho_gia_dinh.id')->select('tinh.name', DB::raw( 'count(*) as total '))->groupBy('tinh.id')->get();
        $dsNu = DB::table('tinh')->join('thanhpho_huyen', 'thanhpho_huyen.id_tinh', '=', 'tinh.id')->join('thitran_xa','thitran_xa.id_thanhpho_huyen', '=', 'thanhpho_huyen.id')->join('ho_gia_dinh', 'ho_gia_dinh.id_thitran_xa', '=', 'thitran_xa.id')->join('cong_dan','cong_dan.id_ho_gia_dinh', '=','ho_gia_dinh.id')->where('gioi_tinh', '=', 'Nữ')->select('tinh.name', DB::raw( 'count(*) as total '))->groupBy('tinh.id')->get();
        $dsNam = DB::table('tinh')->join('thanhpho_huyen', 'thanhpho_huyen.id_tinh', '=', 'tinh.id')->join('thitran_xa','thitran_xa.id_thanhpho_huyen', '=', 'thanhpho_huyen.id')->join('ho_gia_dinh', 'ho_gia_dinh.id_thitran_xa', '=', 'thitran_xa.id')->join('cong_dan','cong_dan.id_ho_gia_dinh', '=','ho_gia_dinh.id')->where('gioi_tinh', '=', 'Nam')->select('tinh.name', DB::raw( 'count(*) as total '))->groupBy('tinh.id')->get();
        $result = array();
        foreach ($ti_le_ds as $danso){
            $people = array($danso->gioi_tinh, $danso->total);
            array_push($result, $people);
        }
        foreach ($ti_le_dan as $key=>$dan){
            $dan->nam = $dsNam[$key]->total;
            $dan->nu = $dsNu[$key]->total;
        }
        return view('user.page.tiledanso',['result'=>$result,'tinhs'=>$tinhs, 'ti_le_dan'=>$ti_le_dan]);
    }
    public function loadTiLeTinh(Request $req){
        $id = $req->id;
        $ti_le_ds = DB::table('cong_dan')->join('ho_gia_dinh','ho_gia_dinh.id','=', 'cong_dan.id_ho_gia_dinh')->join('thitran_xa', 'thitran_xa.id', '=', 'ho_gia_dinh.id_thitran_xa')->join('thanhpho_huyen', 'thanhpho_huyen.id', '=', 'thitran_xa.id_thanhpho_huyen')-> join('tinh', 'tinh.id', '=', 'thanhpho_huyen.id_tinh')->where('tinh.id','=', $id)->select('gioi_tinh', DB::raw('count(*) as total'))->groupBy('gioi_tinh')->get();
        return $ti_le_ds;
    }
    public function getDanhSachCongDanChuaLamCMT(){
        $dsTinh = DB::table('tinh')->get();
        return view('user.page.danhsachchualamcmt', ['dsTinh'=>$dsTinh]);
    }
    public function loadChuaLamCMT(Request $req){
        $id = $req->id;
        $list_cong_dan = DB::table('cong_dan')->join('ho_gia_dinh','ho_gia_dinh.id','=','cong_dan.id_ho_gia_dinh')->join('thitran_xa','thitran_xa.id','=','ho_gia_dinh.id_thitran_xa')->where('thitran_xa.id','=',$id)->select('cong_dan.id','cong_dan.ho_ten','cong_dan.ngay_sinh','cong_dan.que_quan','ho_gia_dinh.sohokhau')->get();
        $result = array();
        $current_year = date('Y');
        foreach ($list_cong_dan as $congdan){
            $year_born = $congdan -> ngay_sinh;
            $split = explode('-',$year_born)[0];
            if( ($split <= ($current_year -18)) && ($split >= ($current_year - 22)) ){
                array_push($result, $congdan);
            }
        }
        return view('user.item.danhsach17',['listCongDan'=>$result])->render();
    }
    public function getDanhSachNghiaVu(){
        return view('user.page.danhsachnghiavu');
    }
}
