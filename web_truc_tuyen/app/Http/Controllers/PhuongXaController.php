<?php

namespace App\Http\Controllers;

use App\ThanhPhoHuyen;
use App\ThiTranXa;
use App\Tinh;
use Illuminate\Http\Request;

class PhuongXaController extends Controller
{
    public function listPhuongXa(){
        $listTinhThanh= Tinh::all();
        $listQuanHuyen= ThanhPhoHuyen::all();
        $listPhuongXa= ThiTranXa::all();
        return view('admin.content.listphuongxa',['listTinhThanh'=>$listTinhThanh,'listQuanHuyen'=>$listQuanHuyen,'listPhuongXa'=>$listPhuongXa]);
    }
    public function getAddPhuongXa()
    {
        $listTinhThanh= Tinh::all();
        $listQuanHuyen= ThanhPhoHuyen::all();
        return view('admin.content.themphuongxa',['listQuanHuyen'=>$listQuanHuyen,'listTinhThanh'=>$listTinhThanh]);

    }

    public function postAddPhuongXa(Request $request)
    {
        $quanhuyen= $request->id_quanhuyen;
        $name=$request->ten_phuongxa;
        $phuongxa= new ThiTranXa;
        $phuongxa->id_thanhpho_huyen=$quanhuyen;
        $phuongxa->name=$name;
        $phuongxa->save();
        return redirect()->back()->with('thongbao', 'Thêm thông tin Phường/Xã/Thị trấn thành công!');
    }
    public function getUpdatePhuongXa($id){
        $listTinhThanh= Tinh::all();
        $listQuanHuyen= ThanhPhoHuyen::all();
        $phuongxa= ThiTranXa::find($id);
        $id_quanhuyen=$phuongxa->id_thanhpho_huyen;
        $quanhuyen= ThanhPhoHuyen::find($id_quanhuyen);
        $id_tinh=$quanhuyen->id_tinh;
        $tinh=Tinh::find($id_tinh);
        return view('admin.content.updatephuongxa',['tinh'=>$tinh,'listQuanHuyen'=>$listQuanHuyen,'phuongxa'=>$phuongxa,'quanhuyen'=>$quanhuyen]);
    }
    public function postUpdatePhuongXa(Request $req, $id){
        $phuongxa= ThiTranXa::find($id);
        $id_quanhuyen= $req->id_quanhuyen;
        $name=$req->ten_phuongxa;
        $phuongxa->id_thanhpho_huyen=$id_quanhuyen;
        $phuongxa->name=$name;
        $phuongxa->save();
        return redirect('phuong_xa/list');
    }
    public function getDeletePhuongXa($id){
        $phuongxa=ThiTranXa::find($id);
        $phuongxa->delete();
        return redirect('/phuong_xa/list');
    }
}
