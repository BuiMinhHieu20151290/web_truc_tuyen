<?php

namespace App\Http\Controllers;

use App\CongDan;
use App\HoGiaDinh;
use App\ThanhPhoHuyen;
use App\ThiTranXa;
use App\Tinh;
use Illuminate\Http\Request;

class CongDanController extends Controller
{
    public function searchCongDan(Request $request)
    {
        $tukhoa = $request->tukhoa;
        $listCongDan = CongDan::where('cmt', 'like','%' .$tukhoa . '%')->get();
        return view('admin.content.listcongdan', ['listCongDan' => $listCongDan,'tukhoa'=>$tukhoa]);

    }
    public function getAddCongDan(){
        $listTinh= Tinh::all();
        $listQuanHuyen=ThanhPhoHuyen::all();
        $listPhuongXa=ThiTranXa::all();
        $listHoGiaDinh=HoGiaDinh::all();
        return view('admin.content.themcongdan',['listHoGiaDinh'=>$listHoGiaDinh,'listTinh'=>$listTinh,'listQuanHuyen'=>$listQuanHuyen,'listPhuongXa'=>$listPhuongXa]);
    }
    public  function postAddCongDan(Request $request){
        $congdan= new CongDan();
        $congdan->ho_ten=$request->ten_congdan;
        $congdan->ngay_sinh=$request->ngay_sinh;
        $congdan->gioi_tinh=$request->gioi_tinh;
        $congdan->que_quan=$request->que_quan;
        $congdan->dan_toc=$request->dan_toc;
        $congdan->quoc_tich=$request->quoc_tich;
        $congdan->cmt=$request->socmnd;
        $congdan->nghe_nghiep=$request->nghe_nghiep;
        $congdan->noi_lam_viec=$request->noilamviec;
        $congdan->vai_tro="null";
        $congdan->id_ho_gia_dinh=$request->id_hogiadinh;
        $congdan->save();
         return redirect()->back()->with('thongbao','Thêm thông tin công dân thành công');
    }
    public function listCongDan(){
        $listCongDan= CongDan::all();
        return view('admin.content.listcongdan',['listCongDan'=>$listCongDan]);
    }
    public function getUpdateCongDan($id){
        $congdan=CongDan::find($id);
        $hogiadinh_id= HoGiaDinh::find($congdan->id_ho_gia_dinh);
        $listHoGiaDinh=HoGiaDinh::all();
        return view('admin.content.updatecongdan',['congdan'=>$congdan,'hogiadinh_id'=>$hogiadinh_id,'listHoGiaDinh'=>$listHoGiaDinh]);
    }
    public function postUpdateCongDan(Request $req, $id){
        $hogiadinh=HoGiaDinh::find($id);
        $ten_sohokhau=$req->ten_sohokhau;
        $id_phuongxa= $req->id_phuongxa;
        $hogiadinh->sohokhau=$ten_sohokhau;
        $hogiadinh->id_thitran_xa=$id_phuongxa;
        $hogiadinh->save();

        return redirect('hogiadinh/list');
    }
    public function getDeleteCongDan($id){
        $congdan=CongDan::find($id);
        $congdan->delete();
        return redirect('/congdan/list');
    }
}
