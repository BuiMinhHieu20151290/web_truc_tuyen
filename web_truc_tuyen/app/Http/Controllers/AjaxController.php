<?php

namespace App\Http\Controllers;

use App\CongDan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class AjaxController extends Controller
{
    //
    public function loadHuyen(Request $req){
        $id= $req->id;
        $dsHuyen= DB::table('thanhpho_huyen')->where('id_tinh','=',$id)->get();
        $result='<option value="">Choose...</option>';
        foreach ($dsHuyen as $huyen){
            $result.='<option value="'.$huyen->id.'">'.$huyen->name.'</option>';
        }
        return $result;
    }
    public function loadThiTran(Request $req){
        $id= $req->id;
        $dsThiTran= DB::table('thitran_xa')->where('id_thanhpho_huyen','=',$id)->get();
        $result='<option value="">Choose...</option>';
        foreach ($dsThiTran as $thitran){
            $result.='<option value="'.$thitran->id.'">'.$thitran->name.'</option>';
        }
        return $result;
    }
    public function loadDanhSachThanhVien(Request $req){
        $id= $req->id;
        $ho_gia_dinh = DB::table('ho_gia_dinh')->where('sohokhau','=',$id)->first();
        $listThanhVien = DB::table('cong_dan')->where('id_ho_gia_dinh','=',$ho_gia_dinh->id)->get();
        $result = '<option value="" selected>Choose...</option>';
        foreach ($listThanhVien as $thanhVien){
            $result .= '<option value="'.$thanhVien->id.'">'.$thanhVien->ho_ten.'</option>';
        }
        return json_encode($result);
    }
    public function loadSoHoKhau(Request $req){
        $id= $req->id;
        $hogd= DB::table('ho_gia_dinh')->join('cong_dan','ho_gia_dinh.id','=','cong_dan.id_ho_gia_dinh')->where('sohokhau','=',$id)->select(['ho_ten','ngay_sinh','gioi_tinh','que_quan','dan_toc','quoc_tich','nghe_nghiep'])->get();
        return view('user.item.danhsachthanhvien',['listThanhVien'=>$hogd])->render();
//        return $hogd;
    }
    public function loadInfoThanhVien(Request $req){
        $hoTen = $req -> hoTen;
        $ngaySinh = $req -> ngaySinh;
        $gioiTinh = $req -> gioiTinh;
        $queQuan = $req ->queQuan;
        $danToc = $req ->danToc;
        $quocTich = $req ->quocTich;
        $idSo = $req -> idSo;
        $hoGiaDinh = DB::table('ho_gia_dinh')->where('sohokhau','=',$idSo)->first();
        $idSoHoKhau = $hoGiaDinh->id;
        $congDan = new CongDan();
        $congDan -> ho_ten = $hoTen;
        $congDan -> ngay_sinh = $ngaySinh;
        $congDan -> gioi_tinh = $gioiTinh;
        $congDan -> que_quan = $queQuan;
        $congDan -> dan_toc = $danToc;
        $congDan -> quoc_tich = $quocTich;
        $congDan -> id_ho_gia_dinh = $idSoHoKhau;
        $congDan ->save();
        $listThanhVien= array();
        array_push($listThanhVien, $congDan);
        return view('user.item.danhsachthanhvien',['listThanhVien'=>$listThanhVien])->render();
    }
    public function loadLamChungMinhThu(Request $req){
        $id= $req->id;
        $hogd= DB::table('ho_gia_dinh')->join('cong_dan','ho_gia_dinh.id','=','cong_dan.id_ho_gia_dinh')->where('sohokhau','=',$id)->select(['cong_dan.id','ho_ten','ngay_sinh','gioi_tinh','que_quan','dan_toc','quoc_tich','nghe_nghiep','cmt'])->get();
        return view('user.item.chungminhthu',['listThanhVien'=>$hogd])->render();
    }
    public function loadIdCongDan(Request $req)
    {
        $id = $req->id;
        $congdan= DB::table('cong_dan')->where('id','=',$id)->get();
        return $congdan;
    }
    public function loadInfoHoKhau(Request $req){
        $id = $req->id;
        $hokhau = DB::table('ho_gia_dinh')->join('cong_dan','cong_dan.id_ho_gia_dinh','=','ho_gia_dinh.id')->where('sohokhau','=',$id)->get();
        return $hokhau;
    }
    public function loadInfoCongDan(Request $req){
        $cmt = $req->id;
        $congdan = DB::table('cong_dan')->where('cmt', $cmt)->get();
        return $congdan;
    }
}
