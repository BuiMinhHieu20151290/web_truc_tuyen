<?php

namespace App\Http\Controllers;

use App\CongDan;
use App\HoGiaDinh;
use App\ThanhPhoHuyen;
use App\ThiTranXa;
use App\Tinh;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthAdminController extends Controller
{
    public function getLogin(){
        return view('admin.auth.login');
    }
    public function postLogin(Request $request){
        $request->validate([
            'email'=>'required',
            'password'=>'required'
        ],[
            'email.required'=>'Bạn cần nhập email',
            'password.required'=>'Bạn cần phải nhập password'
        ]);

        $auth=array('email'=>$request->email,'password'=>$request->password);
        if(Auth::attempt($auth)){
            return redirect('/tinh_thanh/add');
        }
        else{
            return redirect()->back()->with('thongbao','Đăng nhập không thành công');
        }
    }
    public function logout(){
        Auth::logout();
        return redirect('/login');
    }
}
