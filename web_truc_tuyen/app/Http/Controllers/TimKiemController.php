<?php

namespace App\Http\Controllers;

use App\HoGiaDinh;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class TimKiemController extends Controller
{
    //
    public function getTimKiemCongDan() {
        return view('user.page.timkiemcongdan');
    }
    public function postTimKiemCongDan(Request $req){
        $cmt= $req->cmt;
        $congdan= DB::table('cong_dan')->where('cmt','=',$cmt)->first();
        $dsTamTru= DB::table('cong_dan')->join('tam_tru','tam_tru.id_cong_dan','=','cong_dan.id')->join('ho_gia_dinh','ho_gia_dinh.id','=','tam_tru.id_ho_gia_dinh')->join('thitran_xa','thitran_xa.id','=','ho_gia_dinh.id_thitran_xa')->where('cmt','=',$cmt)->get(['ngay_den','ngay_di','ly_do','name']);
        return view('user.page.timkiemcongdan',['congdan'=>$congdan,'dsTamTru'=>$dsTamTru]);
    }
    public function getTimKiemHoGiaDinh(){
        return view('user.page.timkiemhogiadinh');
    }
    public function postTimKiemHoGiaDinh(Request $req){
        $soHoKhau= $req->sohokhau;
        $hogd= DB::table('ho_gia_dinh')->join('cong_dan','ho_gia_dinh.id','=','cong_dan.id_ho_gia_dinh')->where('sohokhau','=',$soHoKhau)->select(['ho_ten','ngay_sinh','gioi_tinh','que_quan','dan_toc','quoc_tich','nghe_nghiep'])->get();
        $listTamTru = DB::table('ho_gia_dinh')->join('tam_tru', 'tam_tru.id_ho_gia_dinh', 'ho_gia_dinh.id')->join('cong_dan', 'cong_dan.id', '=', 'tam_tru.id_cong_dan')->where('sohokhau','=',$soHoKhau)->select('ho_ten','cmt','ngay_den','ngay_di','ly_do')->get();
        return view('user.page.timkiemhogiadinh',['listCongDan'=>$hogd, 'listTamTru'=>$listTamTru]);
//        print_r($listTamTru);
    }
    public function getDanhSachHo(){
        $dsTinh= DB::table('tinh')->get();
        return view('user.page.timkiemdanhsachho',['dsTinh'=>$dsTinh]);
    }
    public function postTimKiemDanhSachHo(Request $req){
        $id= $req->id;
        $dsHo= DB::table('ho_gia_dinh')->where('id_thitran_xa','=',$id)->get();
        $result='';
        foreach ($dsHo as $key=>$ho){
            $congDan = DB::table('cong_dan')->where('id_ho_gia_dinh', '=', $ho->id)->first();
            if($congDan !=null){
                $result .= '<tr>';
                $result .= '<td>'.($key).'</td>';
                $result .= '<td>'.$ho->sohokhau.'</td>';
                $result .= '<td>'.$congDan->ho_ten.'</td>';
                $result .= '<td>'.$congDan->que_quan.'</td>';
                $result .= view('user.item.link', ['ho'=>$ho])->render();
                $result .= '</tr>';
            }
        }
        return $result;
    }
    public function getTimKiemDanhSachTamTru(){
        $dsTinh = DB::table('tinh')->get();
        return view('user.page.timkiemdanhsachtamtru', ['dsTinh'=>$dsTinh]);
    }
    public function loadTimKiemTamTru(Request $req) {
        $id = $req->id;
        $result = DB::table('thitran_xa')->join('ho_gia_dinh','ho_gia_dinh.id_thitran_xa', '=', 'thitran_xa.id')->join('tam_tru', 'tam_tru.id_ho_gia_dinh', '=', 'ho_gia_dinh.id')->join('cong_dan', 'cong_dan.id', '=', 'tam_tru.id_cong_dan')->where('thitran_xa.id', '=', $id)->select('tam_tru.id','sohokhau','ho_ten', 'cmt', 'ngay_den', 'ngay_di', 'ly_do')->get();
        $data = '';
        foreach ($result as $key=>$item){
            $data.= '<tr>';
            $data.= '<td>'.($key + 1).'</td>';
            $data.= '<td scope="row">'.$item->sohokhau.'</td>';
            $data.= '<td>'.$item->ho_ten.'-'.$item->cmt.'</td>';
            $data.= '<td>'.$item->ly_do.'</td>';
            $data.= '<td>'.$item->ngay_den.'</td>';
            $data.= '<td>'.$item->ngay_di.'</td>';
            $data.= '<td class="text-center " style="font-size: 20px"><a href="'.route('chiTietTamTru', ['id'=>$item->id]).'" style="color: inherit;"><i class="fas fa-search-plus"></i></a></td>';
            $data.= '</tr>';
        }
        print_r($result);
        return $data;
//        return $result;
    }
    public function chiTietTamTru (Request $req) {
        $id = $req->id;
        $chuHo = DB::table('tam_tru')->join('ho_gia_dinh','tam_tru.id_ho_gia_dinh', '=', 'ho_gia_dinh.id')->join('cong_dan', 'cong_dan.id_ho_gia_dinh', '=','ho_gia_dinh.id')->where('tam_tru.id', '=', $id)->select('sohokhau', 'que_quan', 'ho_ten')->first();
        $nguoiTamTru = DB::table('tam_tru')->join('cong_dan','cong_dan.id', '=', 'tam_tru.id_cong_dan')->where('tam_tru.id', '=', $id)->first();
//        print_r($chuHo);
//        print_r($nguoiTamTru);
        return view('user.page.chitiettamtru', ['chuHo'=>$chuHo, 'info'=>$nguoiTamTru]);
    }

}
