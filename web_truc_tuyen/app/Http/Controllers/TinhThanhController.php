<?php

namespace App\Http\Controllers;

use App\Tinh;
use Illuminate\Http\Request;

class TinhThanhController extends Controller
{
    public function listTinhThanh(){
        $listTinh= Tinh::all();
        return view('admin.content.listtinhthanh',['listTinh'=>$listTinh]);
    }
    public function getAddTinhThanh()
    {
        return view('admin.content.themtinh');
    }

    public function postAddTinhThanh(Request $request)
    {
        $name = $request->ten_tinh;
        $tinh = new Tinh();
        $tinh->name = $name;
        $tinh->save();
        return redirect()->back()->with('thongbao', 'Thêm thông tin tỉnh,TP trực thuộc trung ương thành công!');
    }
    public function getUpdateTinhThanh($id){
        $tinh= Tinh::find($id);
        return view('admin.content.updatetinh',['tinh'=>$tinh]);
    }
    public function postUpdateTinhThanh(Request $req, $id){
        $tinh= Tinh::find($id);
        $name= $req->ten_tinh;
        $tinh->name=$name;
        $tinh->save();
        return redirect('tinh_thanh/list');
    }
    public function getDeleteTinhThanh($id){
        $tinh=Tinh::find($id);
        $tinh->delete();
        return redirect('/tinh_thanh/list');
    }

}
