<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashBoardController extends Controller
{
    //
    public function index(){
        $ti_le_ds = DB::table('cong_dan')->select('gioi_tinh', DB::raw('count(*) as total'))->groupBy('gioi_tinh')->get();
        $tinhs = DB::table('tinh')->get();
        $ti_le_dan = DB::table('tinh')->join('thanhpho_huyen', 'thanhpho_huyen.id_tinh', '=', 'tinh.id')->join('thitran_xa','thitran_xa.id_thanhpho_huyen', '=', 'thanhpho_huyen.id')->join('ho_gia_dinh', 'ho_gia_dinh.id_thitran_xa', '=', 'thitran_xa.id')->join('cong_dan','cong_dan.id_ho_gia_dinh', '=','ho_gia_dinh.id')->select('tinh.name', DB::raw( 'count(*) as total '))->groupBy('tinh.id')->get();
        $dsNu = DB::table('tinh')->join('thanhpho_huyen', 'thanhpho_huyen.id_tinh', '=', 'tinh.id')->join('thitran_xa','thitran_xa.id_thanhpho_huyen', '=', 'thanhpho_huyen.id')->join('ho_gia_dinh', 'ho_gia_dinh.id_thitran_xa', '=', 'thitran_xa.id')->join('cong_dan','cong_dan.id_ho_gia_dinh', '=','ho_gia_dinh.id')->where('gioi_tinh', '=', 'Nữ')->select('tinh.name', DB::raw( 'count(*) as total '))->groupBy('tinh.id')->get();
        $dsNam = DB::table('tinh')->join('thanhpho_huyen', 'thanhpho_huyen.id_tinh', '=', 'tinh.id')->join('thitran_xa','thitran_xa.id_thanhpho_huyen', '=', 'thanhpho_huyen.id')->join('ho_gia_dinh', 'ho_gia_dinh.id_thitran_xa', '=', 'thitran_xa.id')->join('cong_dan','cong_dan.id_ho_gia_dinh', '=','ho_gia_dinh.id')->where('gioi_tinh', '=', 'Nam')->select('tinh.name', DB::raw( 'count(*) as total '))->groupBy('tinh.id')->get();
        $result = array();
        foreach ($ti_le_ds as $danso){
            $people = array($danso->gioi_tinh, $danso->total);
            array_push($result, $people);
        }
//        return view('user.page.dashboard', ['result'=>$result,'tinhs'=>$tinhs, 'ti_le_dan'=>$ti_le_dan]);
//        print_r($dsNu);
//        print_r($dsNam);
//        print_r($ti_le_dan);
        foreach ($ti_le_dan as $key=>$dan){
            $dan->nam = $dsNam[$key]->total;
            $dan->nu = $dsNu[$key]->total;
        }
//        print_r($ti_le_dan);
        return view('user.page.dashboard', ['result'=>$result,'tinhs'=>$tinhs, 'ti_le_dan'=>$ti_le_dan]);
    }
}
