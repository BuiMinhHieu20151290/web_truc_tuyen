<?php

namespace App\Http\Controllers;

use App\HoGiaDinh;
use App\ThiTranXa;
use App\Tinh;
use App\ThanhPhoHuyen;
use Illuminate\Http\Request;

class HoGiaDinhController extends Controller
{

    //
    public function listHoGiaDinh(){
        $listTinhThanh= Tinh::all();
        $listQuanHuyen= ThanhPhoHuyen::all();
        $listPhuongXa= ThiTranXa::all();
        $listHoGiaDinh=HoGiaDinh::all();
        return view('admin.content.listhogiadinh',['listTinhThanh'=>$listTinhThanh,'listQuanHuyen'=>$listQuanHuyen,'listPhuongXa'=>$listPhuongXa,'listHoGiaDinh'=>$listHoGiaDinh]);
    }
    public function getAddHoGiaDinh()
    {
        $listTinhThanh= Tinh::all();
        $listQuanHuyen= ThanhPhoHuyen::all();
        $listPhuongXa= ThiTranXa::all();
        return view('admin.content.themhogiadinh',['listQuanHuyen'=>$listQuanHuyen,'listTinhThanh'=>$listTinhThanh,'listPhuongXa'=>$listPhuongXa]);

    }

    public function postAddHoGiaDinh(Request $request)
    {
        $phuongxa= $request->id_phuongxa;
        $ten_sohokhau=$request->ten_sohokhau;
        $hogiadinh= new HoGiaDinh();
        $hogiadinh->sohokhau=$ten_sohokhau;
        $hogiadinh->id_thitran_xa=$phuongxa;
        $hogiadinh->save();
        return redirect()->back()->with('thongbao', 'Thêm thông tin hộ gia đình thành công!');
    }
    public function getUpdateHoGiaDinh($id){
       /* $listTinhThanh= Tinh::all();
        $listQuanHuyen= ThanhPhoHuyen::all();
        $listPhuongXa= ThiTranXa::all();*/
        $hogiadinh=HoGiaDinh::find($id);
        $phuongxa=ThiTranXa::find($hogiadinh->id_thitran_xa);
        $quanhuyen=ThanhPhoHuyen::find($phuongxa->id_thanhpho_huyen);
        $tinh=Tinh::find($quanhuyen->id_tinh);
        return view('admin.content.updatehogiadinh',['tinh'=>$tinh,'quanhuyen'=>$quanhuyen,'phuongxa'=>$phuongxa,'hogiadinh'=>$hogiadinh]);
    }
    public function postUpdateHoGiaDinh(Request $req, $id){
        $hogiadinh=HoGiaDinh::find($id);
        $ten_sohokhau=$req->ten_sohokhau;
        $id_phuongxa= $req->id_phuongxa;
        $hogiadinh->sohokhau=$ten_sohokhau;
        $hogiadinh->id_thitran_xa=$id_phuongxa;
        $hogiadinh->save();
        return redirect('hogiadinh/list');
    }
    public function getDeleteHoGiaDinh($id){
        $hogiadinh=HoGiaDinh::find($id);
        $hogiadinh->delete();
        return redirect('/hogiadinh/list');
    }
}
