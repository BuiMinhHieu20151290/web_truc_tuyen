<?php

namespace App\Http\Controllers;

use App\CongDan;
use App\HoGiaDinh;
use App\TamTru;
use App\Tinh;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NghiepVuController extends Controller
{
    //
    public function getLamCMT(){
        return view('user.page.chungminhthu');
    }
    public function postLamCMT(Request $req){
        $id = $req->idCongDan;
        $cmt = $req->cmt;
        $congdan= CongDan::find($id);
        $congdan->cmt= $cmt;
        $congdan->save();
        return redirect('/cmt')->with(['thongbao'=>'Thêm thông tin thành công']);
    }
    public function getGiayKhaiSinh(){
        return view('user.page.giaykhaisinh');
    }
    public function getTamTruTamVang(){
        return view('user.page.tamtrutamvang');
    }
    public function getTachKhau(){
        $dsTinh= Tinh::all();
        return view('user.page.tachkhau', ['dsTinh'=>$dsTinh]);
    }
    public function postTachKhau(Request $req){
        $hokhauold = $req->hokhauold;
        $congdan = $req->congdan;
        $hokhaunew = $req->hokhaunew;
        $thitran = $req->thitran;
        $hoGiaDinh = new HoGiaDinh();
        $hoGiaDinh->sohokhau = $hokhaunew;
        $hoGiaDinh->id_thitran_xa = $thitran;
        $hoGiaDinh->save();
        $congDan = CongDan::find($congdan);
        $congDan->id_ho_gia_dinh = $hoGiaDinh->id;
        $congDan->save();
        return redirect()->back()->with(['thongbao'=>'Tách khẩu thành công']);
    }
    public function dangKiTamTru(Request $req){
        $sohokhau = $req->so_ho_khau;
        $hoGiaDinh = DB::table('ho_gia_dinh')->where('sohokhau','=',$sohokhau)->get();
        $idHoGiaDinh = $hoGiaDinh[0]->id;
        $cmt = $req->cmt;
        $congDan = DB::table('cong_dan')->where('cmt', $cmt)->get();
        $idCongDan = $congDan[0]->id;
        $ly_do = $req->ly_do;
        $ngay_den = $req->ngay_den;
        $ngay_di = $req->ngay_di;
        $ghi_chu = $req->ghi_chu;

        $tamtru = new TamTru();
        $tamtru->id_ho_gia_dinh = $idHoGiaDinh;
        $tamtru->id_cong_dan = $idCongDan;
        $tamtru->ly_do = $ly_do;
        $tamtru->ngay_den = $ngay_den;
        $tamtru->ngay_di = $ngay_di;
        if($ghi_chu){
            $tamtru->ghi_chu = $ghi_chu;
        }
        if($tamtru->save()){
            return redirect()->back()->with(['thongbao'=>'Thêm thông tin thành công']);
        }else{
            return redirect()->back()->with(['thongbao'=>'Thêm thông tin thất bại']);
        }
    }
}
