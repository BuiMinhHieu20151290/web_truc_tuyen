<?php

namespace App\Http\Controllers;

use App\ThanhPhoHuyen;
use App\Tinh;
use Illuminate\Http\Request;

class QuanHuyenController extends Controller
{
    //
    public function listQuanHuyen(){
        $listTinhThanh= Tinh::all();
        $listQuanHuyen= ThanhPhoHuyen::all();
        return view('admin.content.listquanhuyen',['listQuanHuyen'=>$listQuanHuyen,'listTinhThanh'=>$listTinhThanh]);
    }
    public function getAddQuanHuyen()
    {
        $listTinhThanh= Tinh::all();
        return view('admin.content.themquanhuyen',['listTinhThanh'=>$listTinhThanh]);

    }

    public function postAddQuanHuyen(Request $request)
    {
        $tinh= $request->id_tinh;
        $name=$request->ten_quanhuyen;
        $quanhuyen= new ThanhPhoHuyen;
        $quanhuyen->id_tinh=$tinh;
        $quanhuyen->name=$name;
        $quanhuyen->save();
        return redirect()->back()->with('thongbao', 'Thêm thông tin Quận/Huyện/Thị xã thành công!');
    }
    public function getUpdateQuanHuyen($id){
        $listTinhThanh= Tinh::all();
        $quanhuyen= ThanhPhoHuyen::find($id);
        $id_tinh=$quanhuyen->id_tinh;
        $tinh= Tinh::find($id_tinh);
        return view('admin.content.updatequanhuyen',['tinh'=>$tinh,'quanhuyen'=>$quanhuyen,'listTinhThanh'=>$listTinhThanh]);
    }
    public function postUpdateQuanHuyen(Request $req, $id){
        $quanhuyen= ThanhPhoHuyen::find($id);
        $name= $req->ten_quanhuyen;
        $tinh=$req->id_tinh;
        $quanhuyen->id_tinh=$tinh;
        $quanhuyen->name=$name;
        $quanhuyen->save();
        return redirect('quan_huyen/list');
    }
    public function getDeleteQuanHuyen($id){
        $quanhuyen=ThanhPhoHuyen::find($id);
        $quanhuyen->delete();
        return redirect('/quan_huyen/list');
    }
}
