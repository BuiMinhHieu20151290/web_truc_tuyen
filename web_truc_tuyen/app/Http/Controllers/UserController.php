<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller

{
    //
    public function listUser()
    {
        $listUser = User::all();
        return view('admin.content.listuser', ['listUser' => $listUser]);
    }

    public function getAddUser()
    {
        return view('admin.content.themuser');

    }

    public function postAddUser(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|min:8|max:20',
            're-password' => 'required|min:8|max:20|same:password'
        ], [
            'email.email' => 'Bạn chưa nhập đúng định dạng email',
            'password.min' => 'Mật khẩu gồm tối thiểu 8 kí tự',
            'password.max' => 'Mật khẩu gồm tối đa 20 kí tự',
            're-password.same' => 'Mật khẩu nhập lại cần phải giống với mật khẩu đã nhập'
        ]);
        $name = $req->name;
        $email = $req->email;
        $password = $req->password;
        $remember_token = Str::random(10);
        $user = new User;
        $user->name = $name;
        $user->email = $email;
        // $user->email_verified_at =$email;
        $user->password = Hash::make($password);
        $user->remember_token = $remember_token;
        $user->save();
        return redirect()->back()->with('thongbao', 'Thêm user thành công!');
    }

    public function getUpdateUser($id)
    {
        /*$listTinhThanh= Tinh::all();
        $quanhuyen= ThanhPhoHuyen::find($id);
        $id_tinh=$quanhuyen->id_tinh;
        $tinh= Tinh::find($id_tinh);
        return view('admin.content.updatequanhuyen',['tinh'=>$tinh,'quanhuyen'=>$quanhuyen,'listTinhThanh'=>$listTinhThanh]);*/
    }

    public function postUpdateUser(Request $req, $id)
    {
        /* $quanhuyen= ThanhPhoHuyen::find($id);
         $name= $req->ten_quanhuyen;
         $tinh=$req->id_tinh;
         $quanhuyen->id_tinh=$tinh;
         $quanhuyen->name=$name;
         $quanhuyen->save();
         return redirect('quan_huyen/list');*/
    }

    public function getDeleteUser($id)
    {
        $user=User::find($id);
        $user->delete();
        return redirect('/users/list');
    }
}
