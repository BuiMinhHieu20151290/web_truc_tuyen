<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HoGiaDinh extends Model
{
    //
    protected $table='ho_gia_dinh';
    public function thitran_xa(){
        return $this->belongsTo('App\ThiTranXa', 'id_thitran_xa','id');
    }
    public function cong_dan(){
        return $this->hasMany('App\CongDan','id_ho_gia_dinh','id');
    }
    public function tam_tru(){
        return $this->hasMany('App\TamTru','id_ho_gia_dinh','id');
    }
}
