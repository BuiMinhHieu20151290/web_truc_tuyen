<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CongDan extends Model
{
    //
    protected $table='cong_dan';
    public function ho_gia_dinh(){
        return $this->belongsTo('App\HoGiaDinh','id_ho_gia_dinh','id');
    }
    public function tam_tru(){
        return $this->hasMany('App\TamTru','id_cong_dan','id');
    }
}
