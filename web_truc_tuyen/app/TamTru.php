<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TamTru extends Model
{
    //
    protected $table='tam_tru';
    public function cong_dan(){
        return $this->belongsTo('App\CongDan','id_cong_dan','id');
    }
    public function ho_gia_dinh(){
        return $this->belongsTo('App\HoGiaDinh','id_ho_gia_','id');
    }
}
