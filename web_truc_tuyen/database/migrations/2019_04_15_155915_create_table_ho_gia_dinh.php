<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHoGiaDinh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ho_gia_dinh', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sohokhau')->unique();
            $table->unsignedBigInteger('id_thitran_xa');
            $table->foreign('id_thitran_xa')->references('id')->on('thitran_xa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ho_gia_dinh');
    }
}
