<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTamTruTamVang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tam_tru', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_ho_gia_dinh');
            $table->foreign('id_ho_gia_dinh')->references('id')->on('ho_gia_dinh');
            $table->unsignedBigInteger('id_cong_dan');
            $table->foreign('id_cong_dan')->references('id')->on('cong_dan');
            $table->string('ly_do');
            $table->string('ngay_den');
            $table->string('ngay_di');
            $table->string('ghi_chu')->default('null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tam_tru');
    }
}
