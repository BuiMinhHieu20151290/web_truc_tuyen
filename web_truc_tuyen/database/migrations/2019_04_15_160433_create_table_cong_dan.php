<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCongDan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cong_dan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ho_ten');
            $table->string('ngay_sinh');
            $table->string('gioi_tinh');
            $table->string('que_quan');
            $table->string('dan_toc');
            $table->string('quoc_tich');
            $table->string('cmt')->default('null')->unique();
            $table->string('nghe_nghiep');
            $table->string('noi_lam_viec');
            $table->string('vai_tro')->default('null')->change();
            $table->unsignedBigInteger('id_ho_gia_dinh');
            $table->foreign('id_ho_gia_dinh')->references('id')->on('ho_gia_dinh');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cong_dan');
    }
}
