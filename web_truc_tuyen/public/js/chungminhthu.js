$('#so-ho').on('input',function () {
    var id=this.value;
    var arg={
        url:'/loadLamChungMinhThu',
        method:'GET',
        dataType:'text',
        data:{id:id},
        success:function (result) {
            $('#list-thanh-vien').html(result);
        },
        error:function (err) {
            console.log(err);
        }
    }
    $.ajax(arg);
});
function loadData(id) {
    var idHo = id.getAttribute('idHo');
    var args={
        type:"GET",
        url:"/loadIdCongDan",
        dataType: "json",
        data: {id: idHo},
        success:function (result) {
            let congDan = result[0];
            console.log(congDan);
            $('#hoTen').val(congDan.ho_ten);
            $('#ngaySinh').val(congDan.ngay_sinh);
            $('#queQuan').val(congDan.que_quan);
            $('#danToc').val(congDan.dan_toc);
            $('#quocTich').val(congDan.quoc_tich);
            $('#idCongDan').val(congDan.id);
        },
        error:function (err) {
            console.log(err);
        }
    }
    $.ajax(args);
}