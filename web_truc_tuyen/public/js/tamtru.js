$('#so-hk').on( 'input', function () {
    let so = $(this).val();
    let args = {
        method: "GET",
        url: "/loadInfoHoKhau",
        data: {id: so},
        dataType: "text",
        success: function (result) {
            ho_khau = JSON.parse(result);
            console.log(ho_khau[0]);
            $('#chu-ho').html('Chủ hộ: '.concat(ho_khau[0].ho_ten))
        },
        error: function (err) {
            console.log(err);
        }
    }
    $.ajax(args);
});
$('#chung-minh-thu').on('input', function () {
    let cmt = $(this).val();
    let args = {
        method: 'GET',
        data: {id: cmt},
        dataType: "text",
        url: '/loadInfoCongDan',
        success: function (result) {
            info = JSON.parse(result);
            console.log(info[0].ho_ten);
            $('#ho-ten').html(info[0].ho_ten);
        },
        error: function (err) {
            console.log(err);
        }
    }
    $.ajax(args);
})