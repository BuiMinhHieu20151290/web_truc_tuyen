$(document).ready(function () {
    $('#tinh').change(function () {
        var id_tinh= this.value;
        var args={
            url:'/loadhuyen',
            method: 'GET',
            dataType: 'text',
            data:{id:id_tinh},
            success:function (result) {
                $('#huyen').html(result);
            },
            error:function (err) {
                console.log(err);
            }
        }
        $.ajax(args);
    });
    $('#huyen').change(function () {
        var id_huyen= this.value;
        var args={
            url:'/loadthitran',
            method: 'GET',
            dataType: 'text',
            data:{id: id_huyen},
            success: function (result) {
                $('#thitran').html(result);
            },
            error:function (err) {
                console.log(err);
            }
        }
        $.ajax(args);
    });
    $('#thitran').change(function () {
        var id=this.value;
        var args={
            url:'/postDanhSachHo',
            method:'GET',
            dataType:'text',
            data:{id:id},
            success:function (result) {
                console.log(result);
            },
            error:function (err) {
                console.log(err);
            }
        }
        $.ajax(args);
    });
    $('#so-ho-khau').on('input',function () {
        var id=this.value;
        var arg={
            url:'/loadSoHoKhau',
            method:'GET',
            dataType:'text',
            data:{id:id},
            success:function (result) {
                console.log(result);
                $('#list-thanh-vien').html(result);
                // console.log(result);
            },
            error:function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    })
    function loadDataThongKe (url) {
        var id= $(this).val();
        console.log(id);
        var args={
            url: url,
            method:'GET',
            data:{id: id},
            dataType: 'text',
            success: function (result) {
                $('#list-cong-dan').html(result);
            },
            error: function (err) {
                console.log(err);
            }
        }
        $.ajax(args);
    }
})