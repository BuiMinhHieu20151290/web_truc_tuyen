$(document).ready(function () {
    $("form").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var so_ho_khau = $('#so-ho-khau').val();
        console.log(so_ho_khau);
        var url = form.attr('action');
        var arg = {
            type : 'GET',
            url : 'loadInfoThanhVien',
            data : form.serialize().concat('&idSo=').concat(so_ho_khau),
            success : function (data) {
                $('#list-thanh-vien').append(data);
                $("form input[type=text]").val('');
                $("form input[type=password]").val('');
            },
            error : function (data) {
                console.log(data);
            }
        };
        $.ajax(arg);
    });
});