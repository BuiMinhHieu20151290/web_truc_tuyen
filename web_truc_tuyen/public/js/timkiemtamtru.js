$('#thitran').change(function () {
    let idThiTran = $(this).val();
    let args = {
        method: "GET",
        dataType: "text",
        data: {id: idThiTran},
        url: "/loadTimKiemTamTru",
        success: function (result) {
            console.log(result);
            $('#content').html(result);
        },
        error: function (err) {
            console.log(err);
        }
    }
    $.ajax(args);
})