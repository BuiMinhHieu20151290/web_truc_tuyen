$('#thitran').change(function () {
    let id = $(this).val();
    let args = {
        method: "GET",
        dataType: "text",
        data: {id: id},
        url: "/postDanhSachHo",
        success: function (result) {
            console.log(result);
            $('#content').html(result);
        },
        error: function (err) {
            console.log(err);
        }
    };
    $.ajax(args);
})