<div class="header">
    <nav class="navbar navbar-expand-sm navbar-dark" style="border-bottom: 0px solid #ececec">
        <div class="logo"><img src={{asset("admin/image/quochuy.jpg")}}></div>
        <div class="slogan" style="text-align: center">
            <h2>HỆ THỐNG QUẢN LÍ DỮ LIỆU CÔNG DÂN ĐIỆN TỬ</h2>
        </div>
        &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
        <div class="dropdown" >
            @if(Auth::check())
                <button type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" >
                    {{Auth::user()->name}}</button>
                <div class="dropdown-menu">
                    <a href="{{route('logout')}}" class="dropdown-item">Logout</a>
                </div>
            @else
                <a href="{{route('login')}}" style="text-decoration: none; color: #5b96ff;margin-right: 10px;">Đăng nhập</a>
            @endif

        </div>
    </nav>
</div>
