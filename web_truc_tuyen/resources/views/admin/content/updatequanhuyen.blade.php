@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/themdulieu.css')}}">
@endsection
@section('content')
    <div class="content-add-data">
        <h2 style="text-align: center">Cập nhật thông tin quận, huyện, thành phố trực thuộc tỉnh</h2>
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
        @endif
        <form action="{{route('updateQuanHuyen',['id'=>$quanhuyen->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group ">
                <label for="id_tinh" class="mr-sm-2">Tên Tỉnh/Thành phố:</label>
                <select class="form-control" name="id_tinh" style="width: 100%px" required>
                    <option value="{{$tinh->id}}">{{$tinh->name}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="ten_quanhuyen">Tên quận, huyện, thành phố trực thuộc tỉnh:</label>
                <input type="text" class="form-control" id="ten_quanhuyen" placeholder="Nhập tên quận/huyện" name="ten_quanhuyen" value="{{$quanhuyen->name}}" required>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary" style="width: 170px; text-align: center; font-weight: bold">Cập nhật quận, huyện</button>
            </div>
        </form>
    </div>
@endsection
