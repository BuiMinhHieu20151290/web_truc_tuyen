@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/themdulieu.css')}}">
@endsection
@section('content')
    <div class="content-add-data">
        <h2 style="text-align: center">Cập nhật thông tin công dân</h2>

        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
        @endif
        <form action="{{route('updateCongDan')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="ten_congdan">Tên công dân:</label>
                <input type="text" class="form-control" id="ten_congdan" placeholder="Nhập tên công dân"
                       name="ten_congdan" value="{{$congdan->ho_ten}}" required>
            </div>
            <div class="form-group">
                <label for="ngaysinh">Ngày sinh:</label>
                <input type="date" class="form-control" id="ngaysinh" placeholder="Nhập ngày sinh"
                       name="ngay_sinh" value="{{$congdan->ngay_sinh}}" required>

            </div>
            <div class="form-group ">
                <label for="gioitinh" class="mr-sm-2">Giới tính:</label>
                <select class="form-control" id="gioitinh" name="gioi_tinh" value="{{$congdan->gioi_tinh}}" style="width: 100%px" required>
                    <option value="Nam">Nam</option>
                    <option value="Nữ">Nữ</option>
                </select>
            </div>
            <div class="form-group ">
                <label for="quequan" class="mr-sm-2">Quê quán:</label>
                <input type="text" class="form-control" id="quequan" value="{{$congdan->que_quan}}" placeholder="Nhập quê quán" name="que_quan" required>

            </div>
            <div class="form-group ">
                <label for="dantoc" class="mr-sm-2">Dân tộc:</label>
                <input type="text" class="form-control" id="dantoc" value="{{$congdan->dan_toc}}" placeholder="Nhập dân tộc" name="dan_toc" required>
            </div>
            <div class="form-group ">
                <label for="quoctich" class="mr-sm-2">Quốc tịch:</label>
                <input type="text" class="form-control" id="quoctich" value="{{$congdan->quoc_tich}}" placeholder="Nhập quốc tịch"
                       name="quoc_tich" required>
            </div>
            <div class="form-group ">
                <label for="socmnd" class="mr-sm-2">Số CMND:</label>
                <input type="text" class="form-control" id="socmnd" value="{{$congdan->cmt}}" placeholder="Nhập số CMND" name="socmnd" required>
            </div>
            <div class="form-group ">
                <label for="nghenghiep" class="mr-sm-2">Nghề nghiệp:</label>
                <input type="text" class="form-control" id="nghenghiep" value="{{$congdan->nghe_nghiep}}" placeholder="Nhập nghề nghiệp"
                       name="nghe_nghiep" required>
            </div>
            <div class="form-group ">
                <label for="noilamviec" class="mr-sm-2">Nơi làm việc:</label>
                <input type="text" class="form-control" id="noilamviec" placeholder="Nhập nơi làm việc"
                       name="noilamviec" value="{{$congdan->noi_lam_viec}}" required>
            </div>
            <div class="form-group ">
                <label for="hogiadinh" class="mr-sm-2">Hộ gia đình:</label>
                <div class=" form-inline form-group ">
                    <label class="ml-4">Tỉnh/TP:</label>
                    <select class="form-control" id="id_tinh" style="width: 140px">
                        <option value="">--Chọn--</option>
                        @foreach($listTinh as $tinhthanh)
                            <option value="{{$tinhthanh->id}}">{{$tinhthanh->name}}</option>
                        @endforeach
                    </select>
                    <label class="ml-4">Quận/huyện:</label>
                    <select class="form-control" id="id_quanhuyen" style="width: 140px">
                        <option value="">--Chọn--</option>
                        @foreach($listQuanHuyen as $quanhuyen)
                            <option value="{{$quanhuyen->id}}">{{$quanhuyen->name}}</option>
                        @endforeach
                    </select>
                    <label class="ml-4">Phường/xã:</label>
                    <select class="form-control" id="id_phuongxa" style="width: 140px">
                        <option value="">--Chọn--</option>
                        @foreach($listPhuongXa as $phuongxa)
                            <option value="{{$phuongxa->id}}">{{$phuongxa->name}}</option>
                        @endforeach
                    </select>
                    <label class="ml-4">Hộ gia đình:</label>
                    <select class="form-control" id="id_phuongxa" value="{{$hogiadinh_id->sohokhau}}" name="id_hogiadinh" style="width: 143px" required>
                        <option value="">--Chọn--</option>
                        @foreach($listHoGiaDinh as $hogiadinh)
                            <option value="{{$hogiadinh->id}}">{{$hogiadinh->sohokhau}}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary"
                        style="width: 170px; text-align: center; font-weight: bold;background-color: #dd8c16">
                    Thêm thông tin
                </button>
            </div>
        </form>
    </div>
@endsection
