@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlidulieu.css')}}">
@endsection
@section('content')
    <div class="quanlibaihat" style="padding: 15px;">
        <h3 style="text-align: center">Danh sách thông tin công dân</h3>
        <div class="form-group">
            <form class="form-inline" action="{{route('searchCongDan',['id'])}}" method="get">
                <input type="hidden" name="_token"{{-- value="{{csrf_token()}}";--}}>
                <input class="form-control mr-sm-2 " name="tukhoa" type="text" placeholder="Nhập thông tin tìm kiếm"
                       size="80">
                <button style="background-color: #f6993f" class="btn btn-success" type="submit">Search</button>
            </form>
        </div>


        <table class="table table-striped" id="danhsachbaihat" style="text-align: center">
            <thead style=" background-color: #c6c8ca">
            <tr style="text-align: center;">
                <th rowspan="2">STT</th>
                <th scope="col" rowspan="2">Họ và tên</th>
                <th scope="col" rowspan="2">Ngày sinh</th>
                <th scope="col" rowspan="2">Giới tính</th>
                <th scope="col" rowspan="2">Quê quán</th>
                <th scope="col" rowspan="2">Dân tộc</th>
                <th scope="col" rowspan="2">Quốc tịch</th>
                <th scope="col" rowspan="2">Số CMND</th>
                <th scope="col" rowspan="2">Nghề nghiệp</th>
                <th scope="col" rowspan="2">Nơi làm việc</th>
                <th scope="col" colspan="2" style="text-align: center" rowspan="1">Quản lí</th>
            </tr>
            <tr style="text-align: center">
                <th style="text-align: center">Cập nhập</th>
                <th>Xóa</th>
            </tr>
            </thead>
            <tbody>
            @foreach($listCongDan as $key=>$congdan)
                <tr>
                    <td>{{$key+1}}</td>
                    <td style="text-align: left; padding-left: 40px">{{$congdan->ho_ten}}</td>
                    <td style="text-align: left; padding-left: 40px">{{$congdan->ngay_sinh}}</td>
                    <td style="text-align: left; padding-left: 40px">{{$congdan->gioi_tinh}}</td>
                    <td style="text-align: left; padding-left: 40px">{{$congdan->que_quan}}</td>
                    <td style="text-align: left; padding-left: 40px">{{$congdan->dan_toc}}</td>
                    <td style="text-align: left; padding-left: 40px">{{$congdan->quoc_tich}}</td>
                    <td style="text-align: left; padding-left: 40px">{{$congdan->cmt}}</td>
                    <td style="text-align: left; padding-left: 40px">{{$congdan->nghe_nghiep}}</td>
                    <td style="text-align: left; padding-left: 40px">{{$congdan->noi_lam_viec}}</td>
                    <td>
                        <a href="{{route('updateCongDan',['id'=>$congdan->id])}}" type="button" class="btn btn-primary "
                           style="background-color: #dd8c16">Sửa</a>
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                data-target="{{'#myModal'.$congdan->id}}" style="background-color:#dd8c16 ">
                            Xóa
                        </button>
                        <div class="modal" id="{{'myModal'.$congdan->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Thông báo</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Bạn có chắc chắn muốn xóa không
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close
                                        </button>
                                        <a href="{{route('deleteCongDan',['id'=>$congdan->id])}}"
                                           class="btn btn-primary">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection
