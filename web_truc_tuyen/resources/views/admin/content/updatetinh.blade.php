@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/themdulieu.css')}}">
@endsection
@section('content')
    <div class="content-add-data">
    <h3 style="text-align: center">Cập nhật thông tin Tỉnh/Thành Phố trực thuộc trung ương</h3>
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
        @endif
        <form action="{{route('updateTinhThanh',['id'=>$tinh->id])}}" method="post" enctype="multipart/form-data">
            @csrf
        <div class="form-group">
            <label for="ten_tinh">Tên Tỉnh/TP:</label>
            <input type="text" class="form-control" id="ten_tinh" placeholder="Nhập tên tỉnh/thành phố" name="ten_tinh" value="{{$tinh->name}}">
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary" style="width: 170px; text-align: center; font-weight: bold">Cập nhật</button>
        </div>
    </form>
    </div>
@endsection
