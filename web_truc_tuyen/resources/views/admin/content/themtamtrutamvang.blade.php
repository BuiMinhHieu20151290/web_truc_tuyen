@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/themdulieu.css')}}">
@endsection
@section('content')
    <div class="content-add-data">
        <h2 style="text-align: center">Thêm thông tin tạm trú/tạm vắng</h2>
        <form action="/action_page.php">
            <div class="form-group">
                <label for="id_hogiadinh">Hộ gia đình:</label>
                <input type="text" class="form-control" id="id_hogiadinh" placeholder="Nhập tên chủ hộ"
                       name="id_hogiadinh">
            </div>
            <div class="form-group">
                <label for="id_congdan">Tên công dân:</label>
                <input type="text" class="form-control" id="id_congdan" placeholder="Nhập tên công dân"
                       name="id_congdan">
            </div>
            <div class="form-group">
                <label for="ngay_di">Ngày đi:</label>
                <input type="date" class="form-control" id="ngay_di" placeholder="Chọn ngày đi" name="ngay_di">
            </div>
            <div class="form-group">
                <label for="ngay_den">Ngày đến:</label>
                <input type="date" class="form-control" id="ngay_den" placeholder="Chọn ngày đến" name="ngay_den">
            </div>
            <div class="form-group">
                <label for="noi_di">Nơi đi:</label>
                <input type="text" class="form-control" id="noi_di" placeholder="Nhập nơi đi" name="noi_di">
            </div>
            <div class="form-group">
                <label for="noi_den">Nơi đến:</label>
                <input type="text" class="form-control" id="noi_den" placeholder="Nhập nơi đến" name="noi_den">
            </div>
            <div class="form-group">
                <label for="ly_do">Lý do:</label>
                <textarea class="form-control" id="ly_do" placeholder="Nhập lý do " name="ly_do"></textarea>
            </div>

            <div class="form-group">
                <label for="ghi_chu">Ghi chú:</label>
                <textarea class="form-control" id="ghi_chu" placeholder="Nhập ghi chú" name="ghi_chu"></textarea>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary"
                        style="width: 170px; text-align: center; font-weight: bold; background-color: #f6993f">Thêm
                    thông tin
                </button>
            </div>


        </form>
    </div>
@endsection
