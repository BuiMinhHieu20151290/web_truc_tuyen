@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/themdulieu.css')}}">
@endsection
@section('content')
    <div class="content-add-data">
        <h2 style="text-align: center">Thêm thông tin quận, huyện, thành phố trực thuộc tỉnh</h2>
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
        @endif
        <form action="{{route('addPhuongXa')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group ">
                <label for="ten_tinhthanh" class="mr-sm-2">Tỉnh, thành phố trực thuộc trung ương</label>
                <select class="form-control" name="id_tinhthanh" style="width: 100%">
                    <option value="">Chọn tên tỉnh thành</option>
                    @foreach($listTinhThanh as $tinhthanh)
                        <option value="{{$tinhthanh->id}}">{{$tinhthanh->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group ">
                <label for="ten_quanhuyen" class="mr-sm-2">Quận, huyện, thành phố trực thuộc tỉnh</label>
                <select class="form-control" name="id_quanhuyen" style="width: 100%px" required>
                    <option value="">Chọn tên quận, huyện, thị xã</option>
                    @foreach($listQuanHuyen as $quanhuyen)
                        <option value="{{$quanhuyen->id}}">{{$quanhuyen->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="ten_phuongxa">Phường, xã, thị trấn:</label>
                <input type="text" class="form-control" id="ten_phuongxa" placeholder="Nhập tên quận/huyện" name="ten_phuongxa" required>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary" style="width: 200spx; text-align: center; font-weight: bold; background-color: #dd8c16">Thêm phường/ xã/ thị trấn</button>
            </div>
        </form>
    </div>
@endsection
