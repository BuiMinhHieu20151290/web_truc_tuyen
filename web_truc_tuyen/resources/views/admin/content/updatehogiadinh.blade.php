@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/themdulieu.css')}}">
@endsection
@section('content')
    <div class="content-add-data">
        <h2 style="text-align: center">Cập nhật thông tin phường/xã/thị trấn</h2>
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
        @endif
        <form action="{{route('updateHoGiaDinh',['id'=>$hogiadinh->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group ">
                <label for="id_tinh" class="mr-sm-2">Tỉnh, thành phố trực thuộc trung ương:</label>
                <select class="form-control" name="id_tinh" style="width: 100%px" required>
                    <option value="{{$tinh->id}}">{{$tinh->name}}</option>
                </select>
            </div>
            <div class="form-group ">
                <label for="id_quanhuyen" class="mr-sm-2">Quận, huyện, thị xã:</label>
                <select class="form-control" name="id_quanhuyen" style="width: 100%px" required>
                    <option value="{{$quanhuyen->id}}">{{$quanhuyen->name}}</option>
                </select>
            </div>
            <div class="form-group ">
                <label for="id_phuongxa" class="mr-sm-2">Phường, xã, thị trấn:</label>
                <select class="form-control" name="id_phuongxa" style="width: 100%px" required>
                    <option value="{{$phuongxa->id}}">{{$phuongxa->name}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="ten_quanhuyen">Tên chủ hộ(kèm theo số hộ khẩu):</label>
                <input type="text" class="form-control" id="ten_sohokhau" placeholder="Nhập tên chủ hộ-số hộ khẩu"
                       name="ten_sohokhau" value="{{$hogiadinh->sohokhau}}" required>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary"
                        style="width: 170px; text-align: center; font-weight: bold">Cập nhật phường, xã
                </button>
            </div>
        </form>
    </div>
@endsection
