@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/themdulieu.css')}}">
@endsection
@section('content')
    <div class="content-add-data">
        <h2 style="text-align: center">Thêm thông tin user</h2>

        @if(count($errors)>0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $message)
                    {{$message}}
                    <br>
                @endforeach
            </div>
        @endif
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
        @endif
        <form action="{{route('addUser')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên người dùng:</label>
                <input type="text" class="form-control" name="name" placeholder="Nhập tên người dùng" required>
                @if($errors->has('name'))

                @endif
            </div>
            <div class="form-group">
                <label>Email:</label>
                <input type="email" class="form-control" placeholder="Nhập email người dùng" name="email" required>
                @if($errors->has('name'))
                    <div class="alert alert-danger">
                        @foreach($errors->get('email') as $message)
                            {{$message}}
                            <br>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Mật khẩu:</label>
                <input type="password" class="form-control" placeholder="Nhập mật khẩu" name="password" required>
                @if($errors->has('name'))
                    <div class="alert alert-danger">
                        @foreach($errors->get('password') as $message)
                            {{$message}}
                            <br>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Nhập lại mật khẩu:</label>
                <input type="password" class="form-control" placeholder="Nhập lại mật khẩu" name="re-password" required>
                @if($errors->has('name'))
                    <div class="alert alert-danger">
                        @foreach($errors->get('re-password') as $message)
                            {{$message}}
                            <br>
                        @endforeach
                    </div>
                @endif
            </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-primary" style="width: 170px; text-align: center; font-weight: bold; background-color: #f6993f">Thêm thông tin</button>
    </div>
        </form>
    </div>
@endsection
