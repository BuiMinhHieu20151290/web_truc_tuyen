@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlidulieu.css')}}">
@endsection
@section('content')
    <div class="quanlibaihat" style="padding: 15px;">
        <h3 style="text-align: center">Danh sách các Tỉnh/Thành Phố trực thuộc trung ương</h3>
        <table class="table table-striped" id="danhsachbaihat" style="text-align: center">
            <thead style=" background-color: #c6c8ca">
            <tr style="text-align: center;">
                <th rowspan="2">STT</th>
                <th scope="col" rowspan="2">Tên Tỉnh/Thành Phố</th>
                <th scope="col" colspan="2" style="text-align: center" rowspan="1">Quản lí</th>
            </tr>
            <tr style="text-align: center">
                <th style="text-align: center">Cập nhập</th>
                <th>Xóa</th>
            </tr>
            </thead>
            <tbody>
            @foreach($listTinh as $key=>$tinh)
                <tr>
                    <td>{{$key+1}}</td>
                    <td style="text-align: left; padding-left: 40px">{{$tinh->name}}</td>
                    <td>
                        <a href="{{route('updateTinhThanh',['id'=>$tinh->id])}}" type="button" class="btn btn-primary "
                           style="background-color: #dd8c16">Sửa</a>
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                data-target="{{'#myModal'.$tinh->id}}" style="background-color:#dd8c16 ">
                            Xóa
                        </button>
                        <div class="modal" id="{{'myModal'.$tinh->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Thông báo</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Bạn có chắc chắn muốn xóa không
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close
                                        </button>
                                        <a href="{{route('deleteTinhThanh',['id'=>$tinh->id])}}"
                                           class="btn btn-primary">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection