@foreach($listThanhVien as $key=>$thanhvien)
    <button data-toggle="collapse" class="btn btn-block btn-primary" data-target="#demo" style="margin-bottom: 10px;">Thành viên: {{$thanhvien->ho_ten}}</button>
    <div id="demo" class="collapse" style="margin-top:3px;padding: 15px; background-color: white" >
        <p>Họ tên: {{$thanhvien->ho_ten}}</p>
        <p>Ngày sinh: {{$thanhvien->ngay_sinh}}</p>
        <p>Giới tính: {{$thanhvien->gioi_tinh}}</p>
        <p>Quê quán: {{$thanhvien->que_quan}}</p>
        <p>Dân tộc: {{$thanhvien->dan_toc}}</p>
        <p>Quốc tịch: {{$thanhvien->quoc_tich}}</p>
        <p>Nghề nghiêp: {{$thanhvien->nghe_nghiep}}</p>

    </div>
    @endforeach
