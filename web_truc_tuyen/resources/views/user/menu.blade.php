<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="#">
            <img src="{{asset('user/images/icon/logo.png')}}" alt="Cool Admin" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li class="has-sub">
                    <a class="js-arrow" href={{route('dashboard')}}>
                        <i class="fas fa-tachometer-alt"></i>Dashboard
                    </a>
                </li>
                <li class="has-sub">
                    <a href="#" class="js-arrow">
                        <i class="fas fa-gavel"></i></i>Nghiệp vụ</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="{{route('giaykhaisinh')}}">Làm giấy khai sinh</a>
                        </li>
                        <li>
                            <a href="{{route('cmt')}}">Làm chứng minh thư</a>
                        </li>
                        <li>
                            <a href="{{route('tamtrutamvang')}}">Làm giấy tạm trú tạm vắng</a>
                        </li>
                        <li>
                            <a href="{{route('tachkhau')}}">Tách khẩu</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="#" class="js-arrow">
                        <i class="fab fa-searchengin"></i>Tìm kiếm</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="{{route('timkiemcongdan')}}">Công dân</a>
                        </li>
                        <li>
                            <a href="{{route('timkiemhogiadinh')}}">Hộ gia đình</a>
                        </li>
                        <li>
                            <a href="{{route('timkiemdanhsachho')}}">Danh sách hộ theo phường/ xã/ thị trấn</a>
                        </li>
                        <li>
                            <a href="{{route('timkiemdanhsachtamtru')}}">Tìm kiếm danh sách tạm trú</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="#" class="js-arrow">
                        <i class="fas fa-chart-line"></i>Thống kê</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="{{route('congdan17')}}">Danh sách các công dân 17 tuổi</a>
                        </li>
                        <li>
                            <a href="{{route('tiledanso')}}">Tỉ lệ dân số</a>
                        </li>
                        <li>
                            <a href="{{route('danhsachchualamcmt')}}">Danh sách công dân thực hiện nghĩa vụ </a>
                        </li>
                        {{--<li>--}}
                            {{--<a href="{{route('danhsachnghiavu')}}">Danh sách gọi đi nghĩa vụ quân sự</a>--}}
                        {{--</li>--}}
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>