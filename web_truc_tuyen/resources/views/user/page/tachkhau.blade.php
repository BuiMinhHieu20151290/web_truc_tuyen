@extends('user.index')
@section('content')
    <div class="container">
        @if(\Illuminate\Support\Facades\Session::has('thongbao'))
            <div class="alert-success alert">{{\Illuminate\Support\Facades\Session::get('thongbao')}}</div>
            @endif
        <form action="/postTachKhau" method="POST">
            @csrf
            <div class="form-group">
                <label>Nhập mã hộ khẩu cũ</label>
                <input type="text" class="form-control" id="ho-khau-old" placeholder="Nhập hộ khẩu" name="hokhauold" required>
            </div>
            <div class="form-group">
                <label>Công dân cần tách khẩu:</label>
                <select name="congdan" id="cong-dan" required class="form-control" >
                    <option value="">Choose...</option>
                </select>
            </div>
            <div class="form-group">
                <label>Nhập mã hộ khẩu mới</label>
                <input type="text" class="form-control" required placeholder="Nhập hộ khẩu" name="hokhaunew">
            </div>
            <div style="display: flex; justify-content: space-between">
                <div class="form-group ">
                    <label>Chọn tỉnh</label>
                    <select id="tinh" class="ml-1" >
                        <option value="" selected>Choose...</option>
                        @foreach($dsTinh as $tinh)
                            <option value="{{$tinh->id}}">{{$tinh->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Chọn huyện, thành phố</label>
                    <select id="huyen" required class="ml-1">
                        <option value="" selected>Choose...</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Chọn xã, thị trấn</label>
                    <select name="thitran" required id="thitran" class="ml-1">
                        <option value="" selected>Choose...</option>
                    </select>
                </div>
            </div>
            <div class="form-group" style="text-align: center">
                <button type="submit" class="btn btn-primary">Tách khẩu</button>
            </div>
        </form>

    </div>
    @endsection
@section('script')
    <script src="{{asset('js/tachkhau.js')}}"></script>
    @endsection