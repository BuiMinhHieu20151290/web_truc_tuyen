@extends('user.index')
@section('content')
    <div class="container">
        <form action="{{route('timkiemcd')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="col">
                    <label style="float: right; margin-top: 5px;">Nhập chứng minh thư:</label>
                </div>
                <div class="col">
                    <input type="text" required class="form-control" name="cmt" style="border-radius: 3px" placeholder="Nhập số chứng minh">
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary mb-2">Submit</button>
                </div>
            </div>
        </form>
        @if(isset($congdan))
            <div class="row" style="margin-top: 15px">
                <div class="col-md-6" style="border-right: 1px solid #cccccc">
                    <h4 class="text-center mb-2">Thông tin cá nhân</h4>
                    <div class="row mb-1" >
                        <div class="col-md-3 font-weight-bold " >Họ tên</div>
                        <div class="col-md-9">{{$congdan->ho_ten}}</div>
                    </div>
                    <div class="row mb-1" >
                        <div class="col-md-3 font-weight-bold " >Ngày sinh</div>
                        <div class="col-md-9">{{$congdan->ngay_sinh}}</div>
                    </div>
                    <div class="row mb-1" >
                        <div class="col-md-3 font-weight-bold " >Giới tính</div>
                        <div class="col-md-9">{{$congdan->gioi_tinh}}</div>
                    </div>
                    <div class="row mb-1" >
                        <div class="col-md-3 font-weight-bold " >Quê quán</div>
                        <div class="col-md-9">{{$congdan->que_quan}}</div>
                    </div>
                    <div class="row mb-1" >
                        <div class="col-md-3 font-weight-bold " >Dân tộc</div>
                        <div class="col-md-9">{{$congdan->dan_toc}}</div>
                    </div>
                    <div class="row mb-1" >
                        <div class="col-md-3 font-weight-bold " >Quốc tịch</div>
                        <div class="col-md-9">{{$congdan->quoc_tich}}</div>
                    </div>
                    <div class="row mb-1" >
                        <div class="col-md-3 font-weight-bold " >Số CMT</div>
                        <div class="col-md-9">{{$congdan->cmt}}</div>
                    </div>
                    <div class="row mb-1" >
                        <div class="col-md-3 font-weight-bold " >Nghề nghiệp</div>
                        <div class="col-md-9">{{$congdan->nghe_nghiep}}</div>
                    </div>
                    <div class="row mb-1" >
                        <div class="col-md-3 font-weight-bold " >Nơi làm việc</div>
                        <div class="col-md-9">{{$congdan->noi_lam_viec}}</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="text-center md-2">Tiểu sử tạm trú, tạm vắng</h4>
                    @if(isset($dsTamTru))
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Ngày đến</th>
                                <th>Ngày đi</th>
                                <th>Lí do</th>
                                <th>Địa chỉ</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dsTamTru as $tamtru)
                                <tr>
                                    <td>{{$tamtru->ngay_den}}</td>
                                    <td>{{$tamtru->ngay_di}}</td>
                                    <td>{{$tamtru->ly_do}}</td>
                                    <td>{{$tamtru->name}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <div style="text-align: center; margin-bottom: 30px;">Danh sách trống</div>
                    @endif
                </div>
            </div>
            @else
            <div style="text-align: center; margin-top: 30px;">Danh sách trống</div>
        @endif
    </div>
    @endsection