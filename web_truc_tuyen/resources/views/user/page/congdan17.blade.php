@extends('user.index')
@section('content')
    <div class="container" >
        <form class="form-inline justify-content-around">
            <div class="form-group ">
                <label>Chọn tỉnh</label>
                <select id="tinh" class="ml-1" >
                    <option value="" selected>Choose...</option>
                    @foreach($dsTinh as $tinh)
                        <option value="{{$tinh->id}}">{{$tinh->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Chọn huyện, thành phố</label>
                <select id="huyen" required class="ml-1">
                    <option value="" selected>Choose...</option>
                </select>
            </div>
            <div class="form-group">
                <label>Chọn xã, thị trấn</label>
                <select id="thitran" class="ml-1">
                    <option value="" selected>Choose...</option>
                </select>
            </div>
        </form>
        <h4 class="text-center mt-3 mb-3">Danh sách công dân tuổi 17</h4>
        <table class="table table-hover">
            <thead>
                <tr >
                    <th scope="col" style="vertical-align: middle">#</th>
                    <th scope="col" style="vertical-align: middle">Họ tên</th>
                    <th scope="col" style="vertical-align: middle">Ngày sinh</th>
                    <th scope="col" style="vertical-align: middle">Địa chỉ</th>
                    <th scope="col" style="vertical-align: middle">Hộ khẩu</th>
                    <th scope="col" class="text-center" style="vertical-align: middle">Chi tiết</th>
                </tr>
            </thead>
            <tbody id="list-cong-dan">
            </tbody>
        </table>
    </div>
@endsection
@section('script')
    <script src="{{asset('js/congdan17.js')}}"></script>
    @endsection