@extends('user.index')
@section('content')
    <div class="container">
        <form action="{{route('timkiemhogd')}}" method="get" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="col">
                    <label style="float: right; margin-top: 5px;">Nhập số hộ khẩu:</label>
                </div>
                <div class="col">
                    <input required type="text" class="form-control" style="border-radius: 3px" name="sohokhau" placeholder="Nhập hộ khẩu">
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary mb-2">Submit</button>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-6">
                <h4 class="text-center font-weight-normal mb-2">Danh sách thành viên</h4>
                <div class="dropdown">
                    @if(isset($listCongDan)&&count($listCongDan)>0)
                        @foreach($listCongDan as $key=>$congdan)
                            <button data-toggle="collapse" class="btn btn-block btn-primary mb-1" data-target="#demo{{$key}}" >Thành viên {{$key+1}}: {{$congdan->ho_ten}}</button>
                            <div id="demo{{$key}}" class="collapse" style="margin-top:3px;padding: 15px; background-color: white" >
                                <p>Họ tên: {{$congdan->ho_ten}} </p>
                                <p>Ngày sinh: {{$congdan->ngay_sinh}} </p>
                                <p>Giới tính: {{$congdan->gioi_tinh}}</p>
                                <p>Quê quán: {{$congdan->que_quan}}</p>
                                <p>Dân tộc: {{$congdan->dan_toc}}</p>
                                <p>Quốc tịch: {{$congdan->quoc_tich}}</p>
                            </div>
                        @endforeach
                    @else
                        <div style="text-align: center; margin-top: 30px;">Danh sách trống</div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <h4 class="text-center font-weight-normal mb-2">Danh sách tạm trú</h4>
                <div class="dropdown">
                    @if(isset($listTamTru)&&count($listTamTru)>0)
                        @foreach($listTamTru as $key=>$tamtru)
                            <button data-toggle="collapse" class="btn btn-block btn-primary mb-1" data-target="#demol{{$key}}" >Thành viên {{$key+1}}: {{$tamtru->ho_ten}}</button>
                            <div id="demol{{$key}}" class="collapse" style="margin-top:3px;padding: 15px; background-color: white" >
                                <p>Họ tên: {{$tamtru->ho_ten}} </p>
                                <p>Ngày đến: {{$tamtru->ngay_den}} </p>
                                <p>Ngày đi: {{$tamtru->ngay_di}}</p>
                                <p>Lý do: {{$tamtru->ly_do}}</p>
                            </div>
                        @endforeach
                    @else
                        <div style="text-align: center; margin-top: 30px;">Danh sách trống</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endsection