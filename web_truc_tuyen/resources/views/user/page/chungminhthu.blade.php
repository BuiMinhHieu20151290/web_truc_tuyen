@extends('user.index')
@section('content')
    <div class="container">
        @if(\Illuminate\Support\Facades\Session::has('thongbao'))
            <div class="alert alert-success">{{\Illuminate\Support\Facades\Session::get('thongbao')}}</div>
        @endif
        <form>
            <div class="form-row">
                <div class="col" style="display: flex;justify-content: center">
                    <label for="so-ho-khau" style="float: right; margin-top: 5px;">Nhập số hộ khẩu:</label>
                    <input type="text" class="form-control" id="so-ho" style="border-radius: 3px; width: 300px" placeholder="Nhập hộ khẩu">
                </div>
            </div>
        </form>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-6" style="border-right: 1px solid #cccccc;">
                <h4 style="text-align: center; margin-bottom: 15px;" >Danh sách thành viên</h4>
                <div class="dropdown" id="list-thanh-vien">

                </div>
            </div>
            <div class="col-md-6">
                <h4 style="text-align: center; margin-bottom: 15px;">Thông tin công dân cần nhập</h4>
                <form action="{{route('chungminhthu')}}" method="POST" >
                    @csrf
                    <input type="hidden" id="idCongDan" name="idCongDan"/>
                    <div class="form-group">
                        <label>Nhập số cmt:</label>
                        <input class="form-control" type="text" placeholder="Nhập cmt" name="cmt" required>
                    </div>
                    <div class="form-group">
                        <label>Họ tên</label>
                        <input class="form-control" type="text" placeholder="Nhập họ tên công dân" id="hoTen" name="hoTen" disabled required>
                    </div>
                    <div class="form-group">
                        <label>Ngày sinh</label>
                        <input class="form-control" type="date" required name="ngaySinh" id="ngaySinh" disabled/>
                    </div>
                    <div class="form-group">
                        <label>Quê quán</label>
                        <input class="form-control" type="text" required name="queQuan" id="queQuan" disabled>
                    </div>
                    <div class="form-group">
                        <label>Dân tộc</label>
                        <input class="form-control" type="text" required name="danToc" id="danToc" disabled>
                    </div>
                    <div class="form-group">
                        <label>Quốc tịch</label>
                        <input class="form-control" type="text" required name="quocTich" id="quocTich" disabled>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" >Thêm thông tin</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection
@section('script')
    <script type="text/javascript" src="{{asset('js/chungminhthu.js')}}"></script>
    @endsection
