@extends('user.index')
@section('content')
    <div class="container">
        <div class="row" >
            <div class="container">
                @if(Session::has('thongbao'))
                    <div class="alert alert-info">{{Session::get('thongbao')}}</div>
                    @endif
                <form action="{{route('dangKiTamTru')}}" method="POST" >
                    @csrf
                    <div class="form-group">
                        <label>Nhập sổ hộ khẩu</label>
                        <input id="so-hk" type="text" name="so_ho_khau" class="form-control" required />
                        <div id="chu-ho" class="mt-1"></div>
                    </div>
                    <div class="form-group">
                        <label>Nhập số chứng minh thư:</label>
                        <input id="chung-minh-thu" type="text" name="cmt" class="form-control" required/>
                        <div id="ho-ten" class="mt-1"></div>
                    </div>
                    <div class="form-group">
                        <label>Lý do</label>
                        <textarea name="ly_do" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Ngày đến</label>
                        <input type="date" name="ngay_den" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <label>Ngày đi</label>
                        <input type="date" name="ngay_di" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <label>Ghi chú</label>
                        <input type="text" name="ghi_chu" class="form-control" />
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-primary" value="Đăng kí">
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection
@section('script')
    <script src="{{asset('js/tamtru.js')}}"></script>
    @endsection