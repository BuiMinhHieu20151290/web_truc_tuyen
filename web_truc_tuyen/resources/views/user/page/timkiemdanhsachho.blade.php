@extends('user.index')
@section('content')
    <div class="container">
        <form class="form-inline justify-content-around">
            <div class="form-group ">
                <label>Chọn tỉnh</label>
                <select id="tinh" class="ml-1" >
                    <option value="" selected>Choose...</option>
                    @foreach($dsTinh as $tinh)
                        <option value="{{$tinh->id}}">{{$tinh->name}}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Chọn huyện, thành phố</label>
                <select id="huyen" required class="ml-1">
                    <option value="" selected>Choose...</option>
                </select>
            </div>
            <div class="form-group">
                <label>Chọn xã, thị trấn</label>
                <select id="thitran" class="ml-1">
                    <option value="" selected>Choose...</option>
                </select>
            </div>
        </form>
        <h4 class="text-center mt-3 mb-3">Danh sách các hộ gia đình</h4>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Số hộ khẩu</th>
                <th scope="col">Chủ hộ</th>
                <th scope="col">Địa chỉ</th>
                <th scope="col" class="text-center">Chi tiết</th>
            </tr>
            </thead>
            <tbody id="content">
            </tbody>
        </table>
    </div>
    @endsection
@section('script')
    <script src="{{asset('js/timkiemdanhsachho.js')}}"></script>
    @endsection