@extends('user.index');
@section('content')
    <div class="container">
        <form  method="get" id="id-form">
            <div class="form-row">
                <div class="col" style="display: flex; align-items: center; justify-content: center">
                    <label for="so-ho-khau" style="float: right; margin-top: 5px;">Nhập số hộ khẩu:</label>
                    <input   type="text" class="form-control" name="sohokhau" id="so-ho-khau" style="border-radius: 3px; width: 300px" placeholder="Nhập hộ khẩu">
                </div>
            </div>
        </form>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-6" style="border-right: 1px solid #cccccc;">
                <h4 style="text-align: center; margin-bottom: 15px;" >Danh sách thành viên</h4>
                <div class="dropdown" id="list-thanh-vien">

                </div>
            </div>
            <div class="col-md-6">
                <h4 style="text-align: center; margin-bottom: 15px;">Thông tin công dân cần nhập</h4>
                <form action="" id="id-form">
                    <div class="form-group">
                        <label>Họ tên</label>
                        <input class="form-control" type="text" placeholder="Nhập họ tên công dân" name="hoTen" required>
                    </div>
                    <div class="form-group">
                        <label>Ngày sinh</label>
                        <input class="form-control" type="date" required name="ngaySinh">
                    </div>
                    <div class="form-group">
                        <label style="margin-right: 20px">Giới tính</label>
                        <div class="form-check-inline">
                            <label class="form-check-label" for="radio1">
                                <input type="radio" class="form-check-input" id="radio1" name="gioiTinh" value="Nam" checked>Nam
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label" for="radio2">
                                <input type="radio" class="form-check-input" id="radio2" name="gioiTinh" value="Nữ">Nữ
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Quê quán</label>
                        <input class="form-control" type="text" required name="queQuan">
                    </div>
                    <div class="form-group">
                        <label>Dân tộc</label>
                        <input class="form-control" type="text" required name="danToc">
                    </div>
                    <div class="form-group">
                        <label>Quốc tịch</label>
                        <input class="form-control" type="text" required name="quocTich">
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-primary" value="Thêm thông tin" >
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection
@section('script')
    <script src="js/giaykhaisinh.js"></script>
    @endsection