<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Title Page-->
    <title>Login</title>
    <link href="{{asset('user/vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">
    <style>
        div.wrapper{
            width: 500px;
            margin: auto;
            margin-top: 100px;
            border:1px solid rgba(119, 119, 119, 0.57);
            border-radius: 3px;
            padding: 20px 20px;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <h4 class="text-center">Đăng nhập</h4>
        <form action="/postLogin" method="post" >
            @csrf
            <div class="form-group">
                <label>Username:</label>
                <input type="text" class="form-control" placeholder="Nhập username">
            </div>
            <div class="form-group">
                <label>Password:</label>
                <input type="password" class="form-control" placeholder="Nhập password">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary btn-block" value="Login">
            </div>
        </form>
    </div>
</body>

</html>
<!-- end document-->