@extends('user.index')
@section('content')
    <div class="container">
        <form class="form-inline justify-content-around">
            <div class="form-group ">
                <label>Chọn tỉnh</label>
                <select class="ml-1" >
                    <option value="" selected>Choose...</option>
                    <option>Hải Dương</option>
                    <option>Hà Nội</option>
                    <option>Hải Phòng</option>
                </select>
            </div>
            <div class="form-group">
                <label>Chọn huyện, thành phố</label>
                <select class="ml-1">
                    <option value="" selected>Choose...</option>
                    <option>Ninh Giang</option>
                    <option>Gia Lộc</option>
                    <option>Tứ kì</option>
                </select>
            </div>
            <div class="form-group">
                <label>Chọn xã, thị trấn</label>
                <select class="ml-1">
                    <option value="" selected>Choose...</option>
                    <option>Gia Lộc</option>
                    <option>Gia Lương</option>
                    <option>Gia Tân</option>
                </select>
            </div>
        </form>
        <h4 class="text-center mt-3 mb-3">Danh sách công dân đi nghĩa vụ</h4>
        <table class="table table-hover">
            <thead>
            <tr >
                <th scope="col" style="vertical-align: middle">#</th>
                <th scope="col" style="vertical-align: middle">Họ tên</th>
                <th scope="col" style="vertical-align: middle">Ngày sinh</th>
                <th scope="col" style="vertical-align: middle">Giới tính</th>
                <th scope="col" style="vertical-align: middle">Địa chỉ</th>
                <th scope="col" style="vertical-align: middle">Hộ khẩu</th>
                <th scope="col" class="text-center" style="vertical-align: middle">Chi tiết</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Bùi Minh Hiếu</td>
                <td>13/12/1997</td>
                <td>Nam</td>
                <th>số 500, Nguyễn Chế Nghĩa</th>
                <th>445461246</th>
                <td class="text-center " style="font-size: 20px"><i class="fas fa-search-plus"></i></td>
            </tr>
            <tr>
                <th scope="row">1</th>
                <td>Bùi Minh Hiếu</td>
                <td>13/12/1997</td>
                <td>Nam</td>
                <th>số 500, Nguyễn Chế Nghĩa</th>
                <th>445461246</th>
                <td class="text-center " style="font-size: 20px"><i class="fas fa-search-plus"></i></td>
            </tr>
            <tr>
                <th scope="row">1</th>
                <td>Bùi Minh Hiếu</td>
                <td>13/12/1997</td>
                <td>Nam</td>
                <th>số 500, Nguyễn Chế Nghĩa</th>
                <th>445461246</th>
                <td class="text-center " style="font-size: 20px"><i class="fas fa-search-plus"></i></td>
            </tr>
            </tbody>
        </table>
    </div>
    @endsection