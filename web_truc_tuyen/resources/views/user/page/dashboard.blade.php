@extends('user.index')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <a href="{{route('timkiemcongdan')}}" style="text-decoration: none; display: block; color: inherit">
                    <div class="content d-flex flex-column justify-content-center align-items-center" style="height: 100px; background-color: rgba(87,96,255,0.48); border-radius: 4px">
                        <i class="fas fa-user-circle" style="font-size: 40px"></i>
                        <div class="mt-2">Công dân</div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="{{route('timkiemhogiadinh')}}" style="text-decoration: none; display: block; color: inherit">
                    <div class="content d-flex flex-column justify-content-center align-items-center" style="height: 100px; background-color: rgba(93,255,77,0.48); border-radius: 4px">
                        <i class="fas fa-home" style="font-size: 40px"></i>
                        <div class="mt-2">Hộ gia đình</div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="{{route('tamtrutamvang')}}" style="text-decoration: none; display: block; color: inherit">
                    <div class="content d-flex flex-column justify-content-center align-items-center" style="height: 100px; background-color: rgba(255,251,114,0.48); border-radius: 4px">
                        <i class="fas fa-user" style="font-size: 40px"></i>
                        <div class="mt-2">Tạm trú</div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="{{route('tachkhau')}}" style="text-decoration: none; display: block; color: inherit">
                    <div class="content d-flex flex-column justify-content-center align-items-center" style="height: 100px; background-color: rgba(87,96,255,0.48); border-radius: 4px">
                        <i class="fas fa-bell" style="font-size: 40px"></i>
                        <div class="mt-2">Tách khẩu</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="container mt-3">
        <div class="row">
            <div class="col-md-6">
                <div id="chartCountry"></div>
            </div>
            <div class="col-md-6 text-center">
                <div id="population_country"></div>
            </div>
        </div>
        <div class="row mt-3" >
            <div class="col-md-12">
                <div id="column-chart"></div>
            </div>
        </div>
    </div>
    <!--google chart api-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="{{asset('user/vendor/jquery-3.2.1.min.js')}}"></script>
    <script >
        $("#pro").change(function () {
            let id = $(this).val();
            console.log(id);
            var args = {
                method: "GET",
                data: {id: id},
                dataType: 'json',
                url: '/loadTiLeTinh',
                success: function (result) {
                    console.log(result);
                    if (result.length > 0) {
                        drawchartProvince([[result[0].gioi_tinh, result[0].total], [result[1].gioi_tinh, result[1].total]]);
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            }
            $.ajax(args);
        })
    </script>
    <script type="text/javascript">

        // Load the Visualization API and the piechart package.
        google.load('visualization', '1.0', {'packages':['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.setOnLoadCallback(drawChartCountry);
        google.setOnLoadCallback(drawchartProvince);
        google.setOnLoadCallback(drawColumnChart);
        google.setOnLoadCallback(drawPopulationCountry);
        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        function drawChartCountry() {
            // Create the data table.
                    {{--let result = JSON.parse({{$result}});--}}
            var data = new google.visualization.DataTable();
            data.addColumn('string','Percentage');
            data.addColumn('number', 'Devices');
            data.addRows([
                ["{{$result[0][0]}}", {{$result[0][1]}}],
                ["{{$result[1][0]}}", {{$result[1][1]}}],
            ]);

            var options = {
                title: 'Tỉ lệ nam nữ cả nước năm 2019',
            };

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('chartCountry'));
            chart.draw(data, options);
        }
        function drawchartProvince(result) {
            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string','Percentage');
            data.addColumn('number', 'Devices');
            data.addRows(result);

            var options = {
                title: 'Tỉ lệ dân số của tỉnh',
            };

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('chartProvince'));
            chart.draw(data, options);
        }
        function drawPopulationCountry() {
            // Create the data table.
            let data = new google.visualization.DataTable();
            data.addColumn('string','Percentage');
            data.addColumn('number', 'Devices');
            let result = [];
            @foreach($ti_le_dan as $dan)
            result.push(['{{$dan->name}}', {{$dan->total}}])
            @endforeach
            data.addRows(result);

            let options = {
                title: 'Tỉ lệ dân số cả nước',
            };

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('population_country'));
            chart.draw(data, options);
        }
        function drawColumnChart() {
            // Some raw data (not necessarily accurate)
            let result = [];
            result.push(['Province', 'Nam', 'Nữ', 'Dân số'])
            @foreach($ti_le_dan as $dan)
                    result.push(['{{$dan->name}}', {{$dan->nam}}, {{$dan->nu}}, {{$dan->total}}])
                @endforeach
            let data = google.visualization.arrayToDataTable(result);

            var options = {
                title : 'Monthly Coffee Production by Country',
                vAxis: {title: 'Number'},
                hAxis: {title: 'Province'},
                seriesType: 'bars',
                series: {5: {type: 'line'}}
            };

            var chart = new google.visualization.ComboChart(document.getElementById('column-chart'));
            chart.draw(data, options);
        }
    </script>
    @endsection
