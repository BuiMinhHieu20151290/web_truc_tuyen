@extends('user.index')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h4 class="font-weight-normal text-center">Thông tin chủ hộ</h4>
                <div class="mt-2 pl-5">
                    <p>
                        Mã sổ: {{$chuHo->sohokhau}}
                    </p>
                    <p>
                        Chủ hộ: {{$chuHo->ho_ten}}
                    </p>
                    <p>
                        Địa chỉ: {{$chuHo->que_quan}}
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <h4 class="font-weight-normal text-center">Thông tin tạm trú</h4>
                <div class="mt-2 pl-5">
                    <p>
                        Họ tên: {{$info->ho_ten}}
                    </p>
                    <p>
                        Chứng minh thư: {{$info->cmt}}
                    </p>
                    <p>
                        Ngày sinh: {{$info->ngay_sinh}}
                    </p>
                    <p>
                        Quê quán: {{$info->que_quan}}
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <h4 class="font-weight-normal text-center">Chi tiết</h4>
                <div class="mt-2 pl-5">
                    <p>
                        Ngày đến: {{$info->ngay_den}}
                    </p>
                    <p>
                        Ngày đi: {{$info->ngay_di}}
                    </p>
                    <p>
                        Lý do: {{$info->ly_do}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    @endsection