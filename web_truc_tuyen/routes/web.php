<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','DashBoardController@index')->name('dashboard');

Route::get('/cmt' , 'NghiepVuController@getLamCMT')->name('cmt');
Route::post('/chungminhthu', 'NghiepVuController@postLamCMT')->name('chungminhthu');
Route::get('/giaykhaisinh', 'NghiepVuController@getGiayKhaiSinh')->name('giaykhaisinh');
Route::get('/tamtrutamvang', 'NghiepVuController@getTamTruTamVang')->name('tamtrutamvang');
Route::post('/dangKiTamTru','NghiepVuController@dangKiTamTru')->name('dangKiTamTru');
Route::get('/tachkhau', 'NghiepVuController@getTachKhau')->name('tachkhau');
Route::post('/postTachKhau', 'NghiepVuController@postTachKhau');

Route::get('/timkiemcongdan' , 'TimKiemController@getTimKiemCongDan')->name('timkiemcongdan');
Route::post('/timkiemcd','TimKiemController@postTimKiemCongDan')->name('timkiemcd');
Route::get('/timkiemhogiadinh' , 'TimKiemController@getTimKiemHoGiaDinh')->name('timkiemhogiadinh');
Route::get('/timkiemhogd','TimKiemController@postTimKiemHoGiaDinh')->name('timkiemhogd');
Route::get('/timkiemdanhsachho' ,'TimKiemController@getDanhSachHo')->name('timkiemdanhsachho');
Route::get('/postDanhSachHo','TimKiemController@postTimKiemDanhSachHo');
Route::get('/timkiemdanhsachtamtru', 'TimKiemController@getTimKiemDanhSachTamTru')->name('timkiemdanhsachtamtru');
Route::get('/loadTimKiemTamTru', 'TimKiemController@loadTimKiemTamTru');
Route::get('/chiTietTamTru', 'TimKiemController@chiTietTamTru')->name('chiTietTamTru');

Route::get('/congdan17','ThongKeController@getDanhSachCongDan17')->name('congdan17');
Route::get('/loadDSCongDan17','ThongKeController@loadDanhSachCongDan17');
Route::get('/tiledanso','ThongKeController@getTiLeDanSo')->name('tiledanso');
Route::get('/loadTiLeTinh','ThongKeController@loadTiLeTinh');
Route::get('/danhsachchualamcmt','ThongKeController@getDanhSachCongDanChuaLamCMT')->name('danhsachchualamcmt');
Route::get('/loadChuaLamCMT','ThongKeController@loadChuaLamCMT');
Route::get('/danhsachnghiavu','ThongKeController@getDanhSachNghiaVu')->name('danhsachnghiavu');

Route::get('/loadhuyen','AjaxController@loadHuyen');
Route::get('/loadthitran','AjaxController@loadThiTran');
Route::get('/loadDanhSachThanhVien','AjaxController@loadDanhSachThanhVien');
Route::get('/loadSoHoKhau','AjaxController@loadSoHoKhau');
Route::get('/loadInfoThanhVien','AjaxController@loadInfoThanhVien');
Route::get('/loadLamChungMinhThu','AjaxController@loadLamChungMinhThu');
Route::get('/loadIdCongDan','AjaxController@loadIdCongDan');
Route::get('/loadInfoHoKhau','AjaxController@loadInfoHoKhau');
Route::get('/loadInfoCongDan','AjaxController@loadInfoCongDan');

Route::get('/getLogin', function (){
    return view('user.page.login');
})->name('login');

Route::get('/page', function () {
    echo "Xin chào!";
});

Route::get('login','AuthAdminController@getLogin')->name('login');
Route::post('login','AuthAdminController@postLogin')->name('login');
Route::get('logout','AuthAdminController@logout')->name('logout');


/*Route::prefix('admin')->group(
    function () {
        Route::get('login','AuthAdminController@getLogin')->name('login');
        Route::post('login','AuthAdminController@postLogin')->name('login');
    }
);*/
Route::prefix('congdan')->group(
    function () {
        Route::get('/add', 'CongDanController@getAddCongDan');
        Route::post('/add', 'CongDanController@postAddCongDan')->name('addCongDan');
        Route::get('/update/{id}', 'CongDanController@getUpdateCongDan')->name('updateCongDan');
        Route::post('/update/{id}', 'CongDanController@postUpdateCongDan')->name('updateCongDan');
        Route::get('/delete/{id}', 'CongDanController@getDeleteCongDan')->name('deleteCongDan');
        Route::get('/list/', 'CongDanController@listCongDan');
        Route::get('/search/{tukhoa}', 'CongDanController@searchCongDan')->name('searchCongDan');
    }
);

Route::prefix('tinh_thanh')->group(
    function () {
        Route::get('/add', 'TinhThanhController@getAddTinhThanh');
        Route::post('/add', 'TinhThanhController@postAddTinhThanh')->name('addTinhThanh');
        Route::get('/update/{id}', 'TinhThanhController@getUpdateTinhThanh')->name('updateTinhThanh');
        Route::post('/update/{id}', 'TinhThanhController@postUpdateTinhThanh')->name('updateTinhThanh');
        Route::get('/delete/{id}', 'TinhThanhController@getDeleteTinhThanh')->name('deleteTinhThanh');
        Route::get('/list/', 'TinhThanhController@listTinhThanh');
    }
);
Route::prefix('hogiadinh')->group(
    function () {
        Route::get('/add', 'HoGiaDinhController@getAddHoGiaDinh');
        Route::post('/add', 'HoGiaDinhController@postAddHoGiaDinh')->name('addHoGiaDinh');
        Route::get('/update/{id}', 'HoGiaDinhController@getUpdateHoGiaDinh')->name('updateHoGiaDinh');
        Route::post('/update/{id}', 'HoGiaDinhController@postUpdateHoGiaDinh')->name('updateHoGiaDinh');
        Route::get('/delete/{id}', 'HoGiaDinhController@getDeleteHoGiaDinh')->name('deleteHoGiaDinh');
        Route::get('/list/', 'HoGiaDinhController@listHoGiaDinh');
    }
);
Route::prefix('quan_huyen')->group(
    function () {
        Route::get('/add', 'QuanHuyenController@getAddQuanHuyen');
        Route::post('/add', 'QuanHuyenController@postAddQuanHuyen')->name('addQuanHuyen');
        Route::get('/update/{id}', 'QuanHuyenController@getUpdateQuanHuyen')->name('updateQuanHuyen');
        Route::post('/update/{id}', 'QuanHuyenController@postUpdateQuanHuyen')->name('updateQuanHuyen');
        Route::get('/delete/{id}', 'QuanHuyenController@getDeleteQuanHuyen')->name('deleteQuanHuyen');
        Route::get('/list/', 'QuanHuyenController@listQuanHuyen');
    }
);
Route::prefix('tamtru_tamvang')->group(
    function () {
        /*Route::get('/add','CongDanController@getAddCongDan');
        Route::post('/add','AlbumController@postAddAlbum')->name('addAlbum');
         Route::get('/update/{id}','AlbumController@getUpdateAlbum')->name('updateAlbum');
         Route::post('/update/{id}','AlbumController@postUpdateAlbum')->name('updateAlbum');
         Route::get('/delete/{id}','AlbumController@getDeleteAlbum')->name('deleteAlbum');
         Route::get('/list/','AlbumController@listAlbum');*/
    }
);
Route::prefix('tinh_thanhpho')->group(
    function () {
        Route::get('/add', 'CongDanController@getAddCongDan');
        Route::post('/add', 'AlbumController@postAddAlbum')->name('addAlbum');
        Route::get('/update/{id}', 'AlbumController@getUpdateAlbum')->name('updateAlbum');
        Route::post('/update/{id}', 'AlbumController@postUpdateAlbum')->name('updateAlbum');
        Route::get('/delete/{id}', 'AlbumController@getDeleteAlbum')->name('deleteAlbum');
        Route::get('/list/', 'AlbumController@listAlbum');
    }
);
Route::prefix('users')->group(
    function () {
        Route::get('/add', 'UserController@getAddUser');
        Route::post('/add', 'UserController@postAddUser')->name('addUser');
        /* Route::get('/update/{id}','AlbumController@getUpdateAlbum')->name('updateAlbum');
         Route::post('/update/{id}','AlbumController@postUpdateAlbum')->name('updateAlbum');*/
        Route::get('/delete/{id}', 'UserController@getDeleteUser')->name('deleteUser');
        Route::get('/list/', 'UserController@listUser');
    }
);
Route::prefix('phuong_xa')->group(
    function () {
        Route::get('/add', 'PhuongXaController@getAddPhuongXa');
        Route::post('/add', 'PhuongXaController@postAddPhuongXa')->name('addPhuongXa');
        Route::get('/update/{id}', 'PhuongXaController@getUpdatePhuongXa')->name('updatePhuongXa');
        Route::post('/update/{id}', 'PhuongXaController@postUpdatePhuongXa')->name('updatePhuongXa');
        Route::get('/delete/{id}', 'PhuongXaController@getDeletePhuongXa')->name('deletePhuongXa');
        Route::get('/list/', 'PhuongXaController@listPhuongXa');
    }
);

